function shwoCV(typeCv){
    switch (typeCv){
        case 'webhelp':
            jQuery('.group-cv-webhelp').show();
            jQuery('.group-cv-document').hide();
            jQuery('.group-cv-media').hide();
            jQuery('.cvLink#webhelp').hide();
            jQuery('.cvLink#media').show();
            jQuery('.cvLink#document').show();

            break;
        case'media':
            jQuery('.group-cv-webhelp').hide();
            jQuery('.group-cv-document').hide();
            jQuery('.group-cv-media').show();
            jQuery('.cvLink#media').hide();
            jQuery('.cvLink#webhelp').show();
            jQuery('.cvLink#document').show();
          
            break;
        default :
            jQuery('.group-cv-webhelp').hide();
            jQuery('.group-cv-document').hide();
            jQuery('.group-cv-media').hide();
            jQuery('.cvLink#document').hide();
            jQuery('.cvLink#media').hide();
            jQuery('.cvLink#webhelp').hide();
            
            break;
    }
    jQuery("input[name='typeCV']").attr('value',typeCv);
    jQuery("input[name='typeCV']").val(typeCv);
    
}

jQuery("document").ready(function(){
    /*Default*/
    jQuery('.group-cv-webhelp').hide();
    //jQuery('.group-cv-document').show();
    jQuery('.group-cv-media').hide();
    //disabled les butons de submit
    jQuery("#edit-postulercond").attr("disabled", "disabled");
    jQuery("#edit-submit").attr("disabled", "disabled");
    jQuery('#monCv input#edit-field-cv-document-und-0-remove-button , #monCv input#edit-field-cv-audio-und-0-remove-button , #monCv input#edit-field-cv-video-und-0-remove-button').ajaxSuccess(function(){
        //activer les buttons de submit
        jQuery("#edit-postulercond").attr("disabled", "");
        jQuery("#edit-submit").attr("disabled", "");
       
    })
    jQuery('#monCv input , #monCv textarea').on('click , change , focus , mousedown',function(event) {
        //activer les buttons de submit
        jQuery("#edit-postulercond").attr("disabled", "");
        jQuery("#edit-submit").attr("disabled", "");
       
    });
    jQuery(".cvLink").click(function(){
        var typeCv= jQuery(this).attr('id');
        shwoCV(typeCv);
        return false;
    });
    var tabCV='document';
    (jQuery("input[name='typeCV']").val())? tabCV=jQuery("input[name='typeCV']").val():jQuery("input[name='typeCV']").val("document");
    
    shwoCV(tabCV);
    var html=jQuery('.form-item-field-cv-audio-und-0 label').html();
    html +='<span class="form-required" title="Ce champ est obligatoire.">*</span>';
    jQuery('.form-item-field-cv-audio-und-0 label').html(html);
    
    html=jQuery('.form-item-field-cv-video-und-0 label').html();
    html +='<span class="form-required" title="Ce champ est obligatoire.">*</span>';
    jQuery('.form-item-field-cv-video-und-0 label').html(html);
    
    html=jQuery('.form-item-field-cv-document-und-0 label').html();
    html +='<span class="form-required" title="Ce champ est obligatoire.">*</span>';
    jQuery('.form-item-field-cv-document-und-0 label').html(html);
       
    //197
    jQuery("#edit-field-experiences-professionnell-und-add-more").val("Ajouter une expérience");
    jQuery("#edit-field-formation-und-add-more").val("Ajouter une formation");
    jQuery("#edit-field-langue-und-add-more").val("Ajouter une langue");
    jQuery('#monCv').ajaxSuccess(function() {
        jQuery("#edit-field-experiences-professionnell-und-add-more").val("Ajouter une expérience");
        jQuery("#edit-field-formation-und-add-more").val("Ajouter une formation");
        jQuery("#edit-field-langue-und-add-more").val("Ajouter une langue");
    });
})
