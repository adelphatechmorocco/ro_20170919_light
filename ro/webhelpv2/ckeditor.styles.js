CKEDITOR.addStylesSet( 'drupal',
[
	{ name : 'Bleu ciel',
		element : 'span',
		attributes : {
		'class' : 'bleu-ciel' }
	},
	{ name : 'Bleu Webhelp',
		element : 'span',
		attributes : {
		'class' : 'bleu-webhelp' }
	},
	{ name : 'Orange',
		element : 'span',
		attributes : {
		'class' : 'orange' }
	},
	{ name : 'Image gauche',
		element : 'img',
		attributes : {
		'class' : 'img-left' }
	},
	{ name : 'Image droite',
		element : 'img',
		attributes : {
		'class' : 'img-right' }
	}
]);