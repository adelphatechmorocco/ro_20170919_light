<?php



function webhelpv2_form_alter(&$form, &$form_state, $form_id) {
      
     
      //  echo '<pre>';
      // print_r($form['keys']);
      // echo '</pre>';


      

       if($form_id == 'views_exposed_form'){
     
      //      echo '<pre>';
      // print_r($form);
      // echo '</pre>';
        $form['keys']['#attributes']['placeholder'] = t('Search');
      }

      if($form_id == 'user_register_form'){
     
        $form['simplenews']['#description'] = t("I would like to receive news from Webhelp Romania");
      }

    if ($form_id == 'search_block_form') {
      
	$form ['actions'] ['submit'] ['#value'] = ''; // Change the text on the submit button
	//$form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/images/search-button.png');
	unset($form ['actions'] ['submit'] ['#attributes'] ['class']);

	$form ['actions'] ['submit'] ['#attributes'] ['class'] [] = 'input_submit';
  //$form ['search_block_form']['#attributes']['placeholder'] = 'Recherche';
	// Add extra attributes to the text box
	// $form ['search_block_form']["#default_value"][] = 'rtyutyurrrr';
    // $form ['search_block_form'] = array('#default_value' => 'fffffffffffff');
// $form ['search_block_form'] ['#attributes'] ['placeholder'] [] = 'Votre clé';
	$form ['search_block_form'] ['#attributes'] ['onblur'] = "if (this.value == '') {this.value = 'Recherche';}";
	$form ['search_block_form'] ['#attributes'] ['onfocus'] = "if (this.value == 'Recherche') {this.value = '';}";
	$form ['search_block_form'] ['#attributes']['class'] [] = 'input_text';
/*
$form['search_block_form']['#attributes'] = array(
            'placeholder' => 'hiiiiiiiiiii'
        );*/

	//	$form['actions']['submit']['#value']='';
	//	$form['actions']['submit']['#attributes']['class'][]='Send';
     //          echo '<pre>';
     // print_r($form);
     // echo '</pre>';
    } elseif ($form_id == 'user_login') {

	$form["name"]['#title'] = t("Identifiant");
	$form["pass"]['#title'] = t("Mot de passe");
	$form["name"]["#description"] = "";
	$form["pass"]["#description"] = "";
	$form['actions']['submit']['#attributes']['class'][] = 'input_submit';
	$form['actions']['submit']['#attributes']["value"] = t("S'authentifier");
	$form ['name'] ['#attributes']['class'] [] = 'input_text';
	$form ['pass'] ['#attributes']['class'] [] = 'input_text';

	$form ['name'] ['#attributes']['value'] [] = 'Login';
	$form ['pass'] ['#attributes']['value'] [] = 'Mot de passe';

	$form ['name'] ['#attributes'] ['onblur'] = "if (this.value == '') {this.value = 'Login';}";
	$form ['name'] ['#attributes'] ['onfocus'] = "if (this.value == 'Login') {this.value = '';}";

	$form ['pass'] ['#attributes'] ['onblur'] = "if (this.value == '') {this.value = 'Mot de passe';}";
	$form ['pass'] ['#attributes'] ['onfocus'] = "if (this.value == 'Mot de passe') {this.value = '';}";

	if ($form['#action'] == '/user' or $form['#action'] == '/user/login')
	    $form['#action'] = '/';
    } elseif ($form_id == 'simplenews_block_form_90') {
        unset($form['update']);
        /**/
	$form ['submit'] = array('#type' => 'submit', '#value' => t(''));
   // $form['actions']['submit']['#src'] = drupal_get_path('theme', 'webhelpv2') . '/images/send-icon.png';
	$form ['submit'] ['#attributes'] ['class'] [] = 'input_img';
	$form ['mail'] = array('#type' => 'textfield', '#title' => '', '#default_value' => t('Your e-mail here'));
	$form ['mail'] ['#attributes'] ['onblur'] = "if (this.value == '') {this.value = '" . t('your email here') . "';}";
	$form ['mail'] ['#attributes'] ['onfocus'] = "if (this.value == '" . t('Votre mail ici') . "') {this.value = '';}";
	$form ['mail'] ['#required'] = TRUE;
	$form ['mail'] ['#attributes'] ['class'] [] = 'input_text';

     //        echo '<pre>';
     // print_r($form);
     // echo '</pre>';

    }elseif ($form_id =='user_pass'){
        $form['#submit'][] = 'webhelpv2_password_redirect';
    }


}

function webhelpv2_preprocess_node(&$variables) {
    if ($variables['view_mode'] == 'mini_cv') {
	$variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__mini_cv';
    }
}

function webhelpv2_preprocess_page(&$variables, $hook) {


    if (isset($variables['node']) && $variables['node']->nid == 118 && !$variables['logged_in']) {
	drupal_goto('user/register');
    } elseif (isset($variables['node']) && $variables['node']->nid == 118 && $variables['logged_in']) {
	drupal_goto('node/add/cv-webhelp');
    }

    $variables['menu_item'] = menu_get_item();
    if ($variables['menu_item']) {
	// Is this a Views page?
	$variables['classes_array'][] = $variables['menu_item']['page_callback'];
    }
    if (isset($variables['node'])) {
	$variables['classes_array'][] = $variables['node']->type;
	if ($variables['node']->type == 'offre_d_emploi')
	    $variables['classes_array'][] = 'evoluer';
    }
    $variables["classes_webhelp"] = "";
    $menuParent = menu_get_active_trail();

    if (!empty($menuParent[1]) && isset($menuParent[1]['link_title'])) {
	$menuParent = $menuParent[1]['link_title'];
	$filter = array('\'' => '', 'é' => 'e', ' ' => '-', '_' => '-', '/' => '-', '[' => '-', ']' => '');
	$variables["classes_webhelp"] = drupal_clean_css_identifier(drupal_strtolower($menuParent), $filter);
    }
	if (isset($variables['node']->type)) {
		$variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
	}
}

//function webhelpv2_menu_link_alter(&$item) {
//
//    //print_r($item);
//    //die;
//}
//
//function webhelpv2_preprocess_menu_link(&$variables) {
//    // the class array is here
//    //print_r(menu_get_item());
//    //print_r($variables['element']);
//    //die;
//}
//
//function webhelpv2_menu_link(array $variables) {
//   // print_r($variables['element']);
//    //die;
//}
function webhelpv2_preprocess_html(&$variables, $hook) {

    if (drupal_is_front_page()) {
	//    print "is front page";
	$meta_description = array(
	    '#type' => 'html_tag',
	    '#tag' => 'meta',
	    '#attributes' => array(
		'name' => 'description',
		'content' => t('Welcome to Webhelp Romania, where we help you build your career and enjoy challenging projects in an inviting environment.')
	    )
	);
	$meta_keywords = array(
	    '#type' => 'html_tag',
	    '#tag' => 'meta',
	    '#attributes' => array(
		'name' => 'keywords',
		'content' => t("centre d’appel, call center telemarketing, call center outsourcing, webhelp, outsourcing call center")
	    )
	);
	$meta_image= array(
	    '#type' => 'html_tag',
	    '#tag' => 'meta',
	    '#attributes' => array(
		'name' => 'og:image',
		'content' => "/images/toto"
	    )
	);
	drupal_add_html_head($meta_image, 'meta_image');
	drupal_add_html_head($meta_description, 'meta_description');
	drupal_add_html_head($meta_keywords, 'meta_keywords');
    }


    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/jquery.min.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/bootstrap.min.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/owl.carousel.min.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/menu.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/wow.min.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/mouse-check-element.min.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js('//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/jquery.counterup.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/videoLightning.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/bootstrap-carousel.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/fancybox/jquery.fancybox.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/jquery.bxslider.js', array('type' => 'file', 'scope' => 'footer'));

   /* SLIDER REVOLUTION 4.x SCRIPTS */

    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/jquery.themepunch.plugins.min.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/jquery.themepunch.revolution.min.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/Placeholders.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/select2.full.min.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/jquery.maskedinput.min.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/jquery.nanoscroller.min.js', array('type' => 'file', 'scope' => 'footer'));

    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/jquery.easypiechart.min.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/jquery.flip.min.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/jquery.scrollbar.min.js', array('type' => 'file', 'scope' => 'footer'));

    drupal_add_js(base_path().drupal_get_path('theme',$GLOBALS['theme']).'/js/main.js', array('type' => 'file', 'scope' => 'footer'));

}

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
function webhelpv2_preprocess_comment(&$variables, $hook) {
    // If comment subjects are disabled, don't display them.

    if (variable_get('comment_subject_field_' . $variables['node']->type, 1) == 0) {
	$variables['title'] = '';
    }
    $uid = $variables["elements"]["#comment"]->uid;
    $utilisateur = user_load($uid);
    $nom = getFieldValue('field_nom', $utilisateur, 'user');
    $prenom = getFieldValue('field_prenom', $utilisateur, 'user');
    // Add pubdate to submitted variable.
    $variables['pubdate'] = '<time pubdate datetime="' . format_date($variables['comment']->created, 'custom', 'c') . '">' . $variables['created'] . '</time>';
    $variables['submitted'] = t('!username replied on !datetime', array('!username' => '<h3 class="author">' . $nom . '  ' . $prenom . '<h3>', '!datetime' => $variables['pubdate']));

    // Zebra striping.
    if ($variables['id'] == 1) {
	$variables['classes_array'][] = 'first';
    }
    if ($variables['id'] == $variables['node']->comment_count) {
	$variables['classes_array'][] = 'last';
    }
    $variables['classes_array'][] = $variables['zebra'];

    $variables['title_attributes_array']['class'][] = 'comment-title';
}

/*function webhelpv2_css_alter(&$css) {
    unset($css [drupal_get_path('module', 'search') . '/search.css']);
}*/

function webhelpv2_webform_mail_headers_3502($form_values, $node, $sid, $cid) {
  $headers = array(
    'Content-Type'  => 'text/html; charset=UTF-8; format=flowed; delsp=yes',
    'X-Mailer'      => 'Drupal Webform (PHP/'. phpversion() .')',
  );
  return $headers;
}


function webhelpv2_theme() {
  return array(
    'cv_webhelp_node_form' => array(
      'arguments' => array('form' => NULL),
      'template' => 'templates/cv-webhelp-node-form',
      'render element' => 'form'
    ),
  );
}
function webhelpv2_password_redirect(&$form, &$form_state){
    $form_state['redirect'] = 'user/login';
}

function block_render($module, $block_id) {
    print($block_id);
    $block = block_load($module, $block_id);
    $block_content = _block_render_blocks(array($block));
    $build = _block_get_renderable_array($block_content);
    $block_rendered = drupal_render($build);
    return $block_rendered;
}


function webhelpv2_preprocess_block(&$vars, $hook) { //change mytheme to your theme name
  $block = $vars['block'];

/*       echo '<pre>';
     print_r($block);
     echo '</pre>';
*/

}