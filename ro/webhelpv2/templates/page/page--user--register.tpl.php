<header class="slide-pages">
    <div class="top-slide top-slide-candidature">
    <span class="shadow-top"></span>
    <span class="shadow-bottom"></span>
        <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
        <div class="titre">
            <h1 class="text-center"><?php print t('Members Area');?></h1>
            <h2 class="text-center"><?php print t("Create your account and apply now")?></h2>
        </div>
    </div>
</header>
<div id="content">
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>

	
    <div class="espace-candidature">
        <div class="container-espace-candidature register">
	        <?php print $messages; ?>
            <div class="identif-left">
                <div class="connecte-toi">
                        <p><?php print t('Login<br />using your social networks');?></p>
                       
                        <ul class="social-media">
                            <li><a href="<?php print url('user/simple-fb-connec')?>t"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="<?php print url('linkedin/login/0')?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
<!--                             <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li> -->
                        </ul>
                    </div>
            </div>
			<?php print render($page['content']); ?>
        </div>
        <?php include './' . path_to_theme() . '/templates/page/block-newsletter.tpl.php'; ?>
