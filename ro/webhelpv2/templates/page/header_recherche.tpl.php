<?php
global $language ;
$lang_name = $language->language;
?>
<header class="slide-pages recherche">
    <div class="top-slide <?php if(!empty($pclass)){ print $pclass; }else{ ?> top-slide-modele<?php } ?>" <?php if(count($node->field_header_image)>0){ ?>style="background: url('<?php print file_create_url($node->field_header_image[LANGUAGE_NONE][0][uri])?>') center 0;"<?php } ?>>
        <span class="shadow-top"></span>
        <span class="shadow-bottom"></span>
        <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
        <div class="titre">
            <?php if(!empty($ptitre)){ print '<h1 class="text-center">'.$ptitre.'</h1>'; }elseif(!empty($node->title)){ print '<h1 class="text-center">'.$node->title.'</h1>'; }?>
        
        </div>
    </div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
</header>