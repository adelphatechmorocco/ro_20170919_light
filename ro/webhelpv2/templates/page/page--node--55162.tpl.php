<?php
global $user;
if ( $user->uid && in_array('administrator', $user->roles) ) {
  
} else {
    drupal_goto('/');
}
function getDateForSpecificDayBetweenDates($startDate,$endDate,$day_number){
    $endDate = strtotime($endDate);
    $days=array('1'=>'Monday','2' => 'Tuesday','3' => 'Wednesday','4'=>'Thursday','5' =>'Friday','6' => 'Saturday','7'=>'Sunday');
    for($i = strtotime($days[$day_number], strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
    $date_array[]=date('Y-m-d',$i);

    return $date_array;
}
if(!empty($_POST['date-du'])){
    $days = getDateForSpecificDayBetweenDates($_POST['date-du'],$_POST['date-au'],$_POST['jour']);
    foreach($days as $day){
        $heures = explode(',',$_POST['heures']);
        foreach($heures as $start){
            $time = strtotime($start);
            $end = date("H:i", strtotime('+30 minutes', $time));
            $node = new stdClass();
            $node->type = 'creneau';
            $node->title = 'CRENEAU-'.$day."-".$start."-".$end;
            
            $node->language = 'fr';
            node_object_prepare($node);

            $node->field_nombre_de_teleconseillers[LANGUAGE_NONE][0]['value'] = $_POST['nbr'];
            $node->field_date_et_heure_de_creneau[LANGUAGE_NONE][0] = array(
                'value' => $day."T".$start,
                'value2' => $day."T".$end,
                'date_type' => 'datetime'
            );
            node_save($node);
        }
    }
    $options = array('query' => array('added' => '1'));
    drupal_goto('ajout-creneaux', $options);
}
?>
<link rel="stylesheet" type="text/css" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.datepick.css">
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.plugin.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.datepick.js"></script>
<header class="slide-pages">
<div class="top-slide top-slide-candidature">
    <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
    <div class="titre">
        <h1 class="text-center">Ajout de créneaux</h1>
    </div>
</div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
</header>
<div id="content">
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>

<div class="espace-candidature">
    <div class="container-espace-candidature">
        <form action="" method="post" class="form-wrapper" name="ajout-creneau">
            <div class="contenu-form">
                <div>
                    <?php if(!empty($_GET['added']) && $_GET['added']==1){ ?>
                        <div class="success" style="width:100%; color:green; text-align:center;font-size: 18px; display: block; float: left; margin-bottom: 100px;">Créneaux ajoutés avec succés!</div>
                    <?php } ?>
                    <p>Ajouts des créneaux<br /><br /></p>
                    
                    <div class="form-group">
                        <input type="text" name="nbr" placeholder="Nombre de conseillers" required="true" class="required" style="width:100%">
                    </div>
                    <div class="form-group">
                        <input type="text" name="date-du" placeholder="Du" required="true" class="required date" style="width:100%">
                    </div>
                    <div class="form-group">
                        <input type="text" name="date-au" placeholder="Au" required="true" class="required date" style="width:100%">
                    </div>
                    <div class="form-group">
                        <select name="jour" required="true" class="required selectpicker form-control" style="width:100%">
                            <option value="">Jour de la semaine</option>
                            <option value="1">Lundi</option>
                            <option value="2">Mardi</option>
                            <option value="3">Mercredi</option>
                            <option value="4">Jeudi</option>
                            <option value="5">Vendredi</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" name="heures" placeholder="Heures (Séparées par des virgules. Ex: 09:00,10:00,16:30)" required="true" class="required" style="width:100%">
                    </div>
                </div>
            </div>
            <div class="buttons">
                <button type="submit" class="postuler btn">Ajouter</button>
            </div>
        </form>
        <div class="entretien-video">
            <a href="#"><p class="text-center">Présentez vous et passez votre entretien en vidéo</p></a>
        </div>
    </div>
</div>

<script>
$('.date').datepick({dateFormat: 'yyyy-mm-dd'});
</script>