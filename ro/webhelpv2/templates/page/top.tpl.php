<?php
global $language;
$lang_name = $language->language;
?>
<div class="content-top-slide">
    <div class="logo-top wow fadeInLeft">
        <a href="<?php print url('<front>');?>"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/logo.png" alt="" class="logo"></a>
        <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/effet-logo.png" alt="" class="fond-logo wow fadeInRight">

    </div>
    <nav class="navbar wow fadeInRight">
        <div class="navbar-header">
            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-2" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-2">
            <?php if($lang_name=='fr'){ ?>
            <ul class="nav navbar-nav main-menu">
                <li class="active"><a href="<?=url('<front>');?>#webhelp">Qui sommes-nous ?</a></li>
                <li>
                    <a href="<?=url('<front>');?>#avenir">Ton avenir avec nous</a>
                    <div class="pages">
                        <a href="<?php print url('nos-secteurs')?>">Nos Secteurs</a>
                        <a href="<?php print url('nos-metiers')?>">Nos Métiers</a>
                        <a href="<?php print url('webhelp-university')?>">Formation</a>
                        <a href="<?php print url('talent-management')?>">Évolution</a>
                    </div>
                </li>
                <li>
                    <a href="<?=url('<front>');?>#debut-famille">Votre nouvelle famille</a>
                    <div class="pages">
                        <a href="<?php print url('bien-etre')?>" >Bien-être</a>
                        <a href="<?php print url('avantages')?>">Avantages et Bénéfices</a>
                        <a href="<?php print url('webhelp-culture')?>">Notre culture</a>
                        <a href="<?php print url('fun')?>">Fun</a>
                       <!-- <a href="<?php print url('fun-0')?>">Solidarité</a> -->
                    </div>
                </li>
				<li><a target="_blank" href="http://blog.webhelp.ro">Blog</a></li>
                <li><a href="<?=url('<front>');?>#aventure">Envie de nous rejoindre ?</a></li>
            </ul>
            <?php }else if($lang_name=='ro'){ ?>
            <ul class="nav navbar-nav main-menu">
                <li class="active"><a href="<?=url('<front>');?>#webhelp">Cine suntem?</a></li>
                <li>
                    <a href="<?=url('<front>');?>#avenir">Viitorul tau alaturi de noi</a>
                    <div class="pages">
                        <a href="<?php print url('sectoare-de-activitate ')?>">Sectoare de activitate</a>
                        <a href="<?php print url('profesii')?>">Profesii</a>
                        <a href="<?php print url('universitatea-webhelp')?>">Formare</a>
                        <a href="<?php print url('talent-management')?>">Evoluţie</a>
                    </div>
                </li>
                <li>
                    <a href="<?=url('<front>');?>#debut-famille">Noua ta familie</a>
                    <div class="pages">
                        <a href="<?php print url('well-being')?>">Well-being</a>
                        <a href="<?php print url('avantaje-si-beneficii')?>">Avantaje si Beneficii</a>
                        <a href="<?php print url('cultura-noastra')?>">Cultura noastra</a>
                        <a href="<?php print url('distractie')?>">Distracție</a>
                       <!-- <a href="<?php print url('well-being')?>">Solidaritate</a> -->
                    </div>
                </li>
				 <li><a href="http://blog.webhelp.ro/">Blog</a></li>
                <li><a href="<?=url('<front>');?>#aventure">Doreşti să ni te alături?</a></li>
            </ul>
            <?php  }else{ ?>
            <ul class="nav navbar-nav main-menu">
                <li class="active"><a href="<?=url('<front>');?>#webhelp">Who We Are</a></li>
                <li>
                    <a href="<?=url('<front>');?>#avenir">Working With Us</a>
                    <div class="pages">
                        <a href="<?php print url('nos-secteurs')?>">Our Sectors</a>
                        <a href="<?php print url('our-people')?>">Our People</a>
                        <a href="<?php print url('webhelp-university')?>">Training</a>
                        <a href="<?php print url('talent-management')?>">Evolution</a>
                    </div>
                </li>
                <li>
                    <a href="<?=url('<front>');?>#debut-famille">Webhelp Way of Working</a>
                    <div class="pages">
                        <a href="<?php print url('work-environment')?>">Work Environment </a>
                        <a href="<?php print url('advantages-benefits')?>">Advantages & Benefits</a>
                        <a href="<?php print url('webhelp-culture')?>">Webhelp Culture</a>
                        <a href="<?php print url('well-being')?>">Well-Being</a>
                     <!--   <a href="<?php print url('well-being')?>">Solidarity</a> -->
                    </div>
                </li>
				 <li><a target="_blank" href="http://blog.webhelp.ro">Blog</a></li>
                <li><a href="<?=url('<front>');?>#aventure">Join Us</a></li>
            </ul>
            <?php } ?>


            <?php /*print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('nav', 'navbar-nav', 'main-menu')), 'heading' => t('Main menu')));*/ ?>
            <?php
            print block_render('locale', 'language');
            ?>
            <ul class="nav navbar-nav espace-membre">
                <?php if ( $user->uid ) { ?>
                    <li>
                        <a href="#0" class="logged_in">
                            <span class="icon-member icon"></span>
                            <?php print t('Welcome');?>
                            <?php
                            $author = user_load($user->uid);
                            print $author->field_prenom[und][0][value];
                            ?>
                        </a>

                        <div class="pages connexion">
                            <a href="<?php print url("cv-webhelp");?>" class="link"><?php print t('My resume');?></a>
                            <a href="<?php print url("user/".$user->uid."/edit");?>" class="link"><?php print t('My profile');?></a>
                            <a href="<?php print url("user/logout");?>" class="link"><?php print t('Logout');?></a>
                        </div>
                    </li>
                <?php }else{ ?>
                    <li>
                        <a href="#"><span class="icon-member icon"></span> <?php print t('Sign in');?></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </nav>
    <?php if(!empty($titre)){ print $titre; } ?>
</div>