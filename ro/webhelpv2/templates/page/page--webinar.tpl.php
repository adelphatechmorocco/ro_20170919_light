<header class="slide-pages">
    <div class="top-slide top-slide-conseil">
        <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
        <div class="titre">
            <h1 class="text-center"><?php if(!empty($node->field_type[LANGUAGE_NONE][0][value])) { print render($node->field_type[LANGUAGE_NONE][0][value]); }else{ print "Nos conseils RH"; } ?></h1>
        </div>
    </div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
</header>
<div id="content">
    
<div class="content-page conseil">
    <div class="top-page-content">
         <?php 
             print $messages; 
         ?>
        <div id="node-body">
             <?php 
             print render($page['content']);
             ?>
        </div>
    </div>
</div>

    <?php include './' . path_to_theme() . '/templates/page/block-newsletter.tpl.php'; ?>
</div>
 


