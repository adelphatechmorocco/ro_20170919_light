<?php
global $user;
if ( $user->uid ) {
  drupal_goto('/cv-webhelp');
}
?>
<header class="slide-pages">
<div class="top-slide top-slide-candidature">
    <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
    <div class="titre">
        <h1 class="text-center"><?php print t('Members Area');?></h1>
        <h2 class="text-center"><?php print t("Complete the form below and Apply now!")?></h2>
    </div>
</div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
</header>
<div id="content">
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
<div class="espace-candidature identif">
    <div class="container-espace-candidature">
	    <?php print $messages; ?>
        <h3 class="text-center"><?php print t('Welcome')?> !</h3>
        <div class="contenu-form">
            <div class="identif-page">
                <div class="identif-left">
                    <div class="connecte-toi">
                        <p><?php print t('Login<br />using your social networks');?></p>
                       
                        <ul class="social-media">
                            <li><a href="<?php print url('user/simple-fb-connect')?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="<?php print url('linkedin/login/0')?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
<!--                             <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li> -->
                        </ul>
                    </div>
                </div>
                <div class="ou">
                    <?php print t('- or -'); ?>
                </div>
                <div class="identif-right">
                    <div class="espace-membre-identif">
                        <p><?php print t('Members Area'); ?></p>
                        <form action="/?q=user" class="form-signin" method="post" id="user-login" accept-charset="UTF-8">
                            <input type="text" class="form-control" name="name" placeholder="<?php print t('Username'); ?>" required="" />
                            <input type="password" class="form-control" name="pass" placeholder="<?php print t('Password'); ?>" required="" />
                            <input type="hidden" name="form_build_id" value="form-1l6PtUZBDfn01DA1yeq8qf6yHk9vxHbPpABnKOE-d3k">
                            <input type="hidden" name="form_id" value="user_login">
                            <input type="hidden" name="lastcookie" value="">
                            <button class="btn btn-lg btn-block" type="submit"><?php print t('Login'); ?></button>
                        </form>
                        <div class="bottom-espace-membre">
                            <a href="<?php print url('user/register');?>"><?php print t('Create your account'); ?></a>
                            <a href="<?php print url('user/password');?>" class="pwd-oublie"><?php print t('Forgotten password'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>