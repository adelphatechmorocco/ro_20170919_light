<?php
global $user;
?>
<!--*********************************************************Slider**********************************************************-->
    <div style="width: 1px; color: red; margin: auto"></div>
    <header class="slider-home slider-home-portugal">
        <?php // region slider
            echo views_embed_view('actualites', 'block_slider');
        ?>

        <div class="top-slide">
            <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
        </div>
        <div class="bottom-nav">

            <?php
            $view = views_get_view('offre_emploi');
            $display_id = 'page';
            $view->set_display($display_id);
            $view->init_handlers();
            $form_state = array(
            'view' => $view,
            'display' => $view->display_handler->display,
            'exposed_form_plugin' => $view->display_handler->get_plugin('exposed_form'),
            'method' => 'get',
            'rerender' => TRUE,
            'no_redirect' => TRUE,
            );
            $form = drupal_build_form('views_exposed_form', $form_state);
            print drupal_render($form);
            ?>

            <ul>
                <li><a href="http://www.webhelp.com" target="_blank"><?php print t('Institutional Website'); ?> | www.webhelp.com</a></li>
            </ul>
        </div>
    </header>
    <div id="content">
        <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>

        <div style="float:left; clear:both; width:100%"><?php print $messages; ?></div>

        <?php print render($page['centre']); ?>

        <?php //include './' . path_to_theme() . '/templates/page/block-newsletter.tpl.php'; ?>

    </div>

    
    <ul class="share-btn hidden" id="dvid">
        <li>
            <a href="<?php if ( $user->uid ) { print url("cv-webhelp"); }else{ print url("user/login"); } ?>">
                <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mail.png" alt="">
            </a>
            <span><?php print t('Express Application'); ?></span>
        </li>
        <!-- <li>
            <a href="#chat">
                <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/bubble.png" alt="">
            </a>
            <span>Pose tes questions, nous répondons</span>
        </li>-->
        <?php if(!empty($_GET['dev'])){ ?>
	        <li>
	            <a href="#call">
	                <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/call.png" alt="">
	            </a>
	            <span class="large">Vous voulez passer votre entretien ?  <br>
	                Nous vous rappelons tout de suite,  <br>
	                ou à <a href="#">l’heure qui vous arrange</a>
	            </span>
	        </li> 
	    <?php } ?>
        <li>
            <a href="#share">
                <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/share.png" alt="">
            </a>
            <ul>
                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#" class="gplus"><i class="fa fa-google-plus"></i></a></li>
            </ul>
        </li>
        <li class="goto">
            <a class="scroll-top" href="#">
                <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/images/prev.png" alt="">
                </a>
        </li>
    </ul>

    

    <!-- share-btn hidden -->
