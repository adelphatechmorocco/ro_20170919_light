<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
  $disabled = 0;
  $start = strtotime($row->field_field_date_et_heure_de_creneau[0]["raw"]["value"]);
  $now = strtotime(date("Y-m-d H:i:s")); 
  if($start<$now || $row->node_status==0) {
      $disabled = 1;
  }
?>
<?php if($disabled==1){ ?>
    <a class="reserved" href="#0" style="cursor:default;">Réservé</a>
<?php }else{ ?> 
    <a class="fancybox fancybox.ajax" href="creneau/creneau_form/<?php print $row->nid?>">Réserver</a>
<?php } ?>
