<?php
$pclass = "top-slide-infographie";
$ptitre = t("Infographies RH");
include './' . path_to_theme() . '/templates/page/header.tpl.php';
?>

<div id="content">
    <?php print $messages; ?>
    <div class="nos-offres-actualites nos-conseil-rh">
        <div class="content-box-gray">
            <!--<div class="top-bloc-newsletter">
                <div class="content-newsletter" style="width: 100%">
                    <p class="text-center">Retrouvez ici, à travers votre espace infographies RH, plus de conseils et de recommandations sur le choix de votre métier, l’orientation de votre carrière, votre épanouissement au travail …</p>
                </div>
            </div>-->
            <div class="offres-actualites conseil-rh">
    	    <?php
    		 print render($page['content']);
    		?>
            </div>
        </div>
    </div>

    <?php include './' . path_to_theme() . '/templates/page/block-newsletter.tpl.php'; ?>

