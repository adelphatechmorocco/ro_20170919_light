<?php
// $Id: views-exposed-form.tpl.php,v 1.4.4.1 2009/11/18 20:37:58 merlinofchaos Exp $
/**
 * @file views-exposed-form.tpl.php
 *
 * This template handles the layout of the views exposed filter form.
 *
 * Variables available:
 * - $widgets: An array of exposed form widgets. Each widget contains:
 * - $widget->label: The visible label to print. May be optional.
 * - $widget->operator: The operator for the widget. May be optional.
 * - $widget->widget: The widget itself.
 * - $button: The submit button for the form.
 *
 * @ingroup views_templates
 */
global $language ;
$lang_name = $language->language;
?>
<?php if (!empty($q)): ?>
  <?php
    // This ensures that, if clean URLs are off, the 'q' is added first so that
    // it shows up first in the URL.
    print $q;
  ?>
<?php endif; ?>

<?php foreach($widgets as $id => $widget): ?>
    <?php print $widget->widget; ?>
<?php endforeach; ?>
<span class="input-group-btn">
<button class="btn" type="submit"><i class="icon icon-search-small"></i></button>
</span>

<?php



if ($lang_name != 'en') {
    print '<script type="text/javascript">';
    print "jQuery(document).ready(function(){";

    $vocabulary = taxonomy_vocabulary_machine_name_load('ville');
    $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
    $i = 1;
    foreach($terms as $term){
        $transterm = $term->name_field[$lang_name][0]["value"];
        print 'document.getElementById("edit-field-ville-tid").options['.$i.']=new Option("'.$transterm.'", "'.$term->tid.'", false, false);';
        $i++;
    }

    $vocabulary = taxonomy_vocabulary_machine_name_load('secteurs');
    $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
    $i = 1;
    foreach($terms as $term){
        $transterm = $term->name_field[$lang_name][0]["value"];
        print 'document.getElementById("edit-field-secteur-tid").options['.$i.']=new Option("'.$transterm.'", "'.$term->tid.'", false, false);';
        $i++;
    }

    $vocabulary = taxonomy_vocabulary_machine_name_load('langues');
    $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
    $i = 1;
    foreach($terms as $term){
        $transterm = $term->name_field[$lang_name][0]["value"];
        print 'document.getElementById("edit-field-langue-offre-tid").options['.$i.']=new Option("'.$transterm.'", "'.$term->tid.'", false, false);';
        $i++;
    }

    $vocabulary = taxonomy_vocabulary_machine_name_load('type_de_poste');
    $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
    $i = 1;
    foreach($terms as $term){
        $transterm = $term->name_field[$lang_name][0]["value"];
        print 'document.getElementById("edit-field-metier-tid").options['.$i.']=new Option("'.$transterm.'", "'.$term->tid.'", false, false);';
        $i++;
    }

    print '});</script>';
}
?>