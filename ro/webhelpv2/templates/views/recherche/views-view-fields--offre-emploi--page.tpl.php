<?php
global $language;
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 //kpr($fields);
?>



<!--
<div class="ville-offre type-actualite">
    <p class="text-center"><?php //print $fields['field_categorie_actualite']->content?></p>
</div>
<div class="text-offres-actualites text-box-gray">
    <div class="actualites-text">
        <h5><?php //print $fields['title']->content?></h5>
    </div>
    <div class="details-box details-actualites orange-link">
        <?php //print $fields['view_node']->content?>
    </div>
</div>
-->

<div class="ville-offre">
    <p class="text-center"><?php 



    
    if($row->_field_data['nid']['entity']->type == 'actualite'){
        print('Actualité');
    }else{
         print $fields['field_ville']->content;
    } 



    ?><br><span style="font-size: 18px !important;"><?php print $fields['field_secteur']->content?></span></p>
</div>
<div class="text-offres-actualites text-box-gray">
    <div class="poste content-gray-box">
        <a href="#"><h5><?php print $fields['title']->content?></h5></a>
        <p><?php print truncate_utf8($fields['field_description_courte_offre']->content,200)?></p>
        <span class="date"><?php print t('Published on '); ?> <?php print $fields['created']->content?></span>
    </div>
    <div class="details-box orange-link">
        <a href='<?php print url("node/".$fields['nid']->content); ?>'><?php print t('Details'); ?></a>
    </div>
</div>

<?php //echo '<pre>',print_r($row),'</pre>'; ?>