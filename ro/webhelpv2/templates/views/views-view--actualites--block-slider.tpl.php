<div id="owl-demo" class="owl-carousel tp-caption">
    <ul>
    	<?php print $rows; ?>
        <li class="item" data-transition="fade" data-slotamount="7">
            <span class="shadow-top"></span>
            <div class="tp-caption small_thin_grey customin customout fullscreenvideo"
                 data-x="center" data-hoffset="134"
                 data-y="center" data-voffset="-6"
                 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                 data-speed="600"
                 data-start="1000"
                 data-easing="Power4.easeOut"
                 data-endspeed="500"
                 data-endeasing="Power4.easeOut"
                 data-autoplay="true"
                 data-nextslideatend="true"
            >
                <video id="videoIntro" class="video-js vjs-default-skin" preload="none" width="100%" height="100%" data-setup="{}" autoplay>
                    <source src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/slide-intro.mp4" type='video/mp4'>
                    <source src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/slide-intro.webm" type='video/webm'>
                    <source src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/slide-intro.ogv" type='video/ogg'>
                </video>

                <div class="trame"></div>
                <span class="shadow-bottom"></span>
                <div class="tp-caption small_thin_grey customin customout"
                     data-x="center" data-hoffset="0"
                     data-y="bottom" data-voffset="-400"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="1200"
                     data-start="1000"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 1"><h2 class="text-center">Webhelp</h2>
                </div>
                <div class="tp-caption small_thin_grey customin customout"
                     data-x="center" data-hoffset="0"
                     data-y="bottom" data-voffset="-340"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="1200"
                     data-start="1600"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 2"><p class="text-center wow fadeInUp"><?php print t('Your talent, our expertise'); ?></p>
                </div>
            </div>
        </li>
    </ul>
</div>


