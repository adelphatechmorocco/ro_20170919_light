<header class="slide-pages">
    <div class="top-slide top-slide-offres" <?php if(count($node->field_header_image)>0){ ?>style="background: url('<?php print file_create_url($node->field_header_image[LANGUAGE_NONE][0][uri])?>');"<?php } ?>>
    <span class="shadow-top"></span>
    <span class="shadow-bottom"></span>
        <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
        <div class="titre">
            <h1 class="text-center"><?php print t('Apply'); ?></h1>
            <h2 class="text-center">Choisissez votre créneau de rappel </h2>
        </div>
    </div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
</header>
<div id="content">
    
<div class="nos-offres-actualites">
    <div class="content-offres-actualites">
        <div class="offres-actualites offres creneau">
        	<?php print $messages; ?>
            <?php
            print render($page['content']);
            ?>
        </div>
    </div>
</div>

    <?php include './' . path_to_theme() . '/templates/page/block-newsletter.tpl.php'; ?>
