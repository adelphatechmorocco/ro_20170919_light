<?php
global $language ;
$lang_name = $language->language;
$el = $elements['bean']['les-etapes-du-recrutement']['#entity'];
$el2 = $elements['bean']['les-etapes-du-recrutement']['field_onglets'];
$n = count($el2['#items']);
?>
<div class="etapes-recrutement etapes-recrutement-portugal postuler-webhelp <?php print $classes; ?>"  id="postuler-webhelp" <?php print $attributes; ?>>
    <?php
    print render($title_suffix);
    ?>
    <div class="container">
        <h2 class="text-center"><?php print $el->title?></h2>
        <p class="text-center" ><?php print $el->field_description_courte[LANGUAGE_NONE]['0']['value']?></p>
        <ul class="nav nav-tabs" role="tablist">
            <?php
            for($i=0; $i<$n; $i++):
            $index = $el2['#items'][$i]['value'];
            $pon=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_on']['#items'][0]['uri']);
            $poff=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_off']['#items'][0]['uri']);
            if($i%2==0){
                $c="fadeInUp";
            }else{
                $c="fadeInDown";
            }
            ?>
            <li class="<?php if($i==0) { ?>active <?php } ?>tab<?php print $i+1?>">
                <a href="#tabcontent<?php print $i+1?>" role="tab" data-toggle="tab">
                    <div class="etape<?php print $i+1?> item-etape wow fadeInUp">
                        <div class="item-etape-content">
                            <div class="img-etape">
                                <img alt="" src="<?php print $poff?>" class="center-block top">
                                <img alt="" src="<?php print $pon?>" class="center-block bottom">
                            </div>
                            <p><?php print $el2[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?></p>
                            <div class="number-etape">
                                <?php print $i+1?>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            <?php endfor; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php
            for($i=0; $i<$n; $i++):
            $index = $el2['#items'][$i]['value'];
            ?>
            <div class="tab-pane fade <?php if($i==0) { ?>active in<?php } ?>" id="tabcontent<?php print $i+1?>">
                <?php print $el2[$i]['entity']['field_collection_item'][$index]['field_description']['#items'][0]['value']?>
            </div>
            <?php endfor; ?>
        </div>
        <div class="img-wh">
            <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-wh.png">
        </div>
    </div>

</div>