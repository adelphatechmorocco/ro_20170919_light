<?php
global $language ;
$lang_name = $language->language;
$el = $elements['bean']['webhelp-tu-connais-']['#entity'];
?>
<div class="webhelp-multinationale who-we-are <?php print $classes; ?>" <?php print $attributes; ?> id="webhelp">
    <?php
    print render($title_suffix);
    ?>
    <div class="content-webhelp-multinationale">
        <div class="text-webhelp-multinationale">
            <h2 class="text-center wow zoomIn"><?php print $el->title; ?></h2>
            <?php print $el->field_description_courte[$lang_name]['0']['value']; ?>

        </div>
        <div class="webhelp-map">
            <div class="items-webhelp-map portugal-map pays-map wow zoomIn">
                <div class="map">
                    <a href="<?php print url($el->field_lien_bouton_pays[$lang_name]['0']['value']); ?>">
                        <img alt="" src="<?php print file_create_url($el->field_carte_du_pays[LANGUAGE_NONE]['0']['uri'])?>" class="icons">
                    </a>
                    <div class="marker-roumanie_1">
                        <div class='pin'></div>
                    </div>
                    <div class="marker-roumanie_2">
                        <div class='pin'></div>
                    </div>  
					<div class="marker-roumanie_3">
                        <div class='pin'></div>
                    </div>
                    <div class="marker-roumanie_4">
                        <div class='pin'></div>
                    </div>
                </div>
                <h4><?php print $el->field_nom_du_pays[$lang_name]['0']['value']; ?></h4>
                <?php print $el->field_description_pays[$lang_name]['0']['value']; ?>
                <a class="link" href="<?php print url($el->field_lien_bouton_pays[$lang_name]['0']['value']); ?>"><?php print $el->field_titre_bouton_pays[$lang_name]['0']['value']; ?></a>
            </div>
            <div class="items-webhelp-map maroc-icon wow fadeInUp">
                <a href="#0" style="cursor: default"><i class="map-marker"></i></a>
                <br/>
                <div id="text-carousel">
                    <!-- Wrapper for slides -->
                    <?php
                    $pays = explode(',',$el->field_liste_des_pays[$lang_name]['0']['value']);
                    ?>
                    <ul>
                        <?php foreach ($pays as $p): ?>
                            <li class="item active">
                                <span><?php print $p?></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="items-webhelp-map multinatinal-map wow zoomIn"  data-wow-duration="2s">
                <div class="map">
                    <a target="_blank" href="<?php print url($el->field_lien_bouton_monde[$lang_name]['0']['value']); ?>">
                        <img alt="" src="<?php print file_create_url($el->field_carte_du_monde[LANGUAGE_NONE]['0']['uri'])?>" class="img-responsive">
                    </a>
                </div>
                <h4><?php print $el->field_titre_carte_monde[$lang_name]['0']['value']; ?></h4>
                <?php print $el->field_description_carte_monde[$lang_name]['0']['value']; ?>
                <a class="link" target="_blank" href="<?php print url($el->field_lien_bouton_monde[$lang_name]['0']['value']); ?>"><?php print $el->field_titre_bouton_monde[$lang_name]['0']['value']; ?></a>
            </div>
        </div>
    </div>
</div>