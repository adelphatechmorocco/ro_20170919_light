<?php 
/*$el = $elements['bean']['comment-postuler-chez-webhelp-']['#entity'];
$el1 = $elements['bean']['comment-postuler-chez-webhelp-']['field_les_plus_1'];
$n = count($el1['#items']);
$el2 = $elements['bean']['comment-postuler-chez-webhelp-']['field_les_plus_2'];
$o = count($el2['#items']);
$el3 = $elements['bean']['comment-postuler-chez-webhelp-']['field_etapes2'];
$p = count($el3['#items']);
?>  
 <div class="postuler-webhelp <?php print $classes; ?>" <?php print $attributes; ?> id="postuler-webhelp">
	 <?php
    print render($title_suffix);
    ?>
    <div class="container">
        <div class="content-postuler-webhelp">
            <h2 class="text-center wow zoomIn"><?php print $el->title?></h2>
            <p class="text-center choix-postuler"><?php print $el->field_sous_titre[LANGUAGE_NONE]['0']['value']?></p>
            <div class="methodes-postuler">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="chrono wow zoomIn">
                        <p><?php print $el->field_nom[LANGUAGE_NONE]['0']['value']?></p>
                        <div class="explication-chrono explication-methode">
                            <?php print $el->field_description_courte[LANGUAGE_NONE]['0']['value']?>
                        </div>
                        <div class="avantages-chrono avantages">
                            <span>Les</span>
                            <?php for($i=0; $i<$n; $i++):
				            $index = $el1['#items'][$i]['value'];
				            ?>
                            <p><span><?php print $el1[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?> : </span><?php print $el1[$i]['entity']['field_collection_item'][$index]['field_description']['#items'][0]['value']?></p>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="online wow zoomIn"  data-wow-duration="2s">
                        <p><?php print $el->field_titre_2[LANGUAGE_NONE]['0']['value']?></p>
                        <div class="explication-online explication-methode">
                            <?php print $el->field_description[LANGUAGE_NONE]['0']['value']?>
                            <p class="astuce"><span>Astuce: </span><?php print $el->field_astuces[LANGUAGE_NONE]['0']['value']?></p>
                        </div>
                        <div class="avantages-online avantages">
                            <span>Les</span>
                            <?php for($i=0; $i<$o; $i++):
				            $index = $el2['#items'][$i]['value'];
				            ?>
                            <p><span><?php print $el2[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?> : </span><?php print $el2[$i]['entity']['field_collection_item'][$index]['field_description']['#items'][0]['value']?></p>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="suite-postuler">
                <h3 class="text-center"><?php print $el->field_titre_suite[LANGUAGE_NONE]['0']['value']?></h3>
                <div class="etapes">
	                <?php for($i=0; $i<$p; $i++):
		            $index = $el3['#items'][$i]['value'];
		            $pon=file_create_url($el3[$i]['entity']['field_collection_item'][$index]['field_picto_on']['#items'][0]['uri']); 
		            ?>
	                    <div class="etape1 item-etape wow zoomIn">
	                        <img alt="" src="<?php print $pon?>" class="center-block">
	                        <div class="text-etape text-center"><?php print $el3[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?></div>
	                    </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
        <div class="img-wh">
            <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-wh.png">
        </div>
    </div>
</div>

*/
?>
<?php
global $language ;
$lang_name = $language->language;
$el = $elements['bean']['get-hired-']['#entity'];
?>
<div class="etapes-recrutement etapes-recrutement-portugal postuler-webhelp <?php print $classes; ?>" <?php print $attributes; ?>> 
    <?php
    print render($title_suffix);
    ?>
    <div class="container">
        <h2 class="text-center">Get Hired !</h2>
        <p class="text-center" >Our intelligent recruitment process attracts the people who can represent our clients in the best<br/>
            possible way. Meaning, we take pride in finding the right people for the right job.</p>
        <ul class="nav nav-tabs" role="tablist">
            <li class="active tabs1">
                <a href="#postuler" role="tab" data-toggle="tab">
                    <div class="etape1 item-etape wow fadeInUp">
                        <div class="item-etape-content">
                            <div class="img-etape">
                                <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-candidature.png" class="center-block top">
                                <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-candidature-active.png" class="center-block bottom">
                            </div>
                            <p>Do you want to <br/>be apart of Webhelp <br/>Portugal Team ?</p>
                            <div class="number-etape">
                                1
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            <li class="tab2">
                <a href="#etude" role="tab"  data-toggle="tab">
                    <div class="etape2 wow zoomIn">
                        <div class="item-etape-content">
                            <div class="img-etape">
                                <img alt="" class="top center-block" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-phone.png">
                                <img alt="" class="bottom center-block" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-phone-active.png">
                            </div>

                            <div class="imgs-cherche imgs-box cf">

                            </div>
                            <p>What is your <br/>next step ?</p>
                            <div class="number-etape">
                                2
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            <li class="tab3">
                <a href="#test" role="tab" data-toggle="tab">
                    <div class="etape3 wow fadeInUp">
                        <div class="item-etape-content">
                            <div class="img-etape">
                                <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-test.png" class="center-block top">
                                <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-test-active.png" class="center-block bottom">
                            </div>
                            <p>How your local <br/>interview runs ?</p>
                            <div class="number-etape">
                                3
                            </div>
                        </div>
                    </div>

                </a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane fade active in" id="postuler">
                <div class="comment-postuler">
                    <h5>Apply in 2 MINUTES :</h5>
                    <div class="item-tab">
                        <p>- Create your account.</p>
                        <p>- Select the sector, city and language.</p>
                        <p>- Select your job offer and join your resume.</p>
                        <p>- Select the perfect position for you in one of the 3 sectors : </p>
                        <p>1- <a href="">Telco & Logistics </a> / 2- <a href="">Travel & Tourism </a> / 3- <a href="">Consumer Goods </a> </p>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="etude">
                <div class="comment-postuler">
                    <h5>Study and review of your application </h5>
                    <div class="item-tab">
                        <p>- We will contact you for a telephone interview</p>
                        <p>- After a successful telephone interview, you will receive linguistic test</p>
                        <p> </p>

                    </div>

                </div>

            </div>
            <div class="tab-pane fade" id="test">
                <div class="comment-postuler">
                    <div class="item-tab">
                        <h5>Based on your location your final interview will be:</h5>
                        <h6>For candidates abroad:</h6>
                        <p>The interview will be done through Skype or telephone<br/>
                        </p>
                        <h6>For candidates who reside in Portugal:</h6>
                        <p>You will have physical individual or group interview at our office to assess and validate your skills and motivation</p>

                    </div>
                </div>
            </div>
        </div>
        <div class="img-wh">
            <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-wh.png">
        </div>
    </div>

</div>