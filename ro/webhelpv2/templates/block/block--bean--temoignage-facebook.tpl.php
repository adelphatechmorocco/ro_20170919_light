<?php
global $language ;
$lang_name = $language->language;
$el = $elements['bean']['temoignage-facebook']['#entity'];
?>

<div class="avis-collaborateurs avis-collaborateurs-pays avis-collaborateurs-portugal <?php print $classes; ?>" <?php print $attributes; ?>">
<?php
print render($title_suffix);
?>
    <div class="container">
        <div class="content-avis wow zoomIn">
            <?php if(!empty($el->field_nom[$lang_name]['0']['value'])){ ?> 
			<i class="fa fa-facebook-square"></i>
			<a href="#">@<?php print $el->field_nom[$lang_name]['0']['value']; ?></a><?php } ?>
            <?php print $el->field_description_courte[$lang_name]['0']['value']; ?>
        </div>
    </div>
</div>