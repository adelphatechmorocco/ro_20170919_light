<?php
global $language ;
$lang_name = $language->language;
$el = $elements['bean']['-laventure-commence']['#entity'];
?>
<div class="aventure-commence aventure-portugal <?php print $classes; ?>" id="aventure" <?php print $attributes; ?>>
    <?php
    print render($title_suffix);
    ?>
    <div class="container">
        <div class="content-aventure-commence">
            <div class="left-aventure">
                <h2 class="wow zoomIn"><?php print $el->title?></h2>
                <p><?php print $el->field_sous_titre[$lang_name]['0']['value']; ?></p>
                <div class="content-recrutement wow zoomIn">
                    <span class="number text-center">1</span>
                    <p class="text-number"><a href="<?php print $el->field_lien[$lang_name]['0']['value']; ?>" style="color:inherit !important"><?php print $el->field_nom[$lang_name]['0']['value']; ?></a></p>
                </div>
                <div class="icon-suite wow fadeInUp">
                    <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/icon1.png">
                    <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/icon2.png">
                    <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/icon3.png">
                </div>
                <div class="content-integration wow zoomIn"  data-wow-duration="2s">
                    <span class="number text-center">2</span>
                    <p class="text-number"><a href="<?php print $el->field_lien_2[$lang_name]['0']['value']; ?>" style="color:inherit !important"><?php print $el->field_titre_2[$lang_name]['0']['value']; ?></a></p>
                </div>
            </div>
            <div class="right-aventure">
                <div class="img-iphone">
	                <?php if($lang_name=="en"){ ?>
                    	<img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/IphoneROVEN.png"  class="img-responsive">
                    <?php }else if($lang_name=="ro"){ ?>
                    	<img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/IphoneRO.png"  class="img-responsive">
                     <?php }else{ ?>
                    	<img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/IphoneROVFR.png"  class="img-responsive">
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>