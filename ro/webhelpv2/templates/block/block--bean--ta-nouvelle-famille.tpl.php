<?php
global $language ;
$lang_name = $language->language;
$el = $elements['bean']['ta-nouvelle-famille']['#entity'];
$el2 = $elements['bean']['ta-nouvelle-famille']['field_boutons'];
$n = count($el2['#items']);
?>
<div class="nouvelle-famille working-way <?php print $classes; ?>" <?php print $attributes; ?> id="famille">
    <?php
    print render($title_suffix);
    ?>
    <div class="content-nouvelle-famille">
        <div class="text-nouvelle-famille">
            <div id="debut-famille"></div>
            <h2 class="wow zoomIn"><?php print $el->title; ?></h2>
            <div class="wow zoomIn"><?php print $el->field_description_courte[$lang_name]['0']['value']; ?></div>
        </div>
        <div class="avantages-webhelp wow zoomIn"  data-wow-duration="2s">
            <?php

            for($i=0; $i<$n; $i++):
                $index = $el2['#items'][$i]['value'];
                $pon=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_on']['#items'][0]['uri']);
                $poff=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_off']['#items'][0]['uri']);
                //kpr($el2[$i]['entity']['field_collection_item'][$index]['field_nom']);
                ?>
                <div class="item">
                    <a href="<?php print url($el2[$i]['entity']['field_collection_item'][$index]['field_lien']['#items'][0]['value'])?>">
                        <div class="imgs-cherche imgs-famille cf">
                            <img alt="" class="top" src="<?php print $poff?>">
                            <img alt="" class="bottom" src="<?php print $pon?>">
                        </div>
                        <div class="text-center"><?php print $el2[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?></div>
                    </a>
                </div>
            <?php endfor; ?>
        </div>
    </div>
</div>