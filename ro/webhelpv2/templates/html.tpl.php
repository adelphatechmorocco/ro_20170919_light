<?php
global $user;
/*global $language ;
$lang_name = $language->language;
if ($user->uid) {
} else {
    if($_SERVER['REDIRECT_URL']=="/".$lang_name."/user"){
        header('Location: '.url('user/login'));
    }
}*/
?>
<!DOCTYPE html>
<!--[if IE 8]>   <html class="ie8"> <![endif]-->
<!--[if IE 9]>   <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<!-- Smartsupp Live Chat script -->
<?php if(!empty($_GET['dev'])){ ?>
<script type="text/javascript">
var _smartsupp = _smartsupp || {};
_smartsupp.key = 'd1a2a51a6d886db9c83ed62a061b26fa6933e267';
window.smartsupp||(function(d) {
    var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
    s=d.getElementsByTagName('script')[0];c=d.createElement('script');
    c.type='text/javascript';c.charset='utf-8';c.async=true;
    c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
})(document);
</script>
<?php } ?>

<head>
    <meta charset="utf-8">
	
	<meta http-equiv="Expires" content="Tue, 01 Jan 1995 12:12:12 GMT">
	<meta http-equiv="Pragma" content="no-cache">
	
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
    <script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
    <link rel="shortcut icon" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/favicon.ico">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/nanoscroller.css">
	<!--[if IE 8]>
	    <link rel="stylesheet" type="text/css" href="<?php print $base_path; ?>sites/all/themes/webhelp/styles/style_ie8.css" media="all" />
	<![endif]-->
	<!--[if IE 7]>
	   <link rel="stylesheet" type="text/css" href="<?php print $base_path; ?>sites/all/themes/webhelp/styles/style_ie7.css" media="all" />
	<![endif]-->

	<!--[if IE 6]>
	<script src="js/png.js" language="javascript"></script>
	<script language="javascript">
	DD_belatedPNG.fix('.png');
	</script>
	    <![endif]-->

</head>
<body>
<?php print $page_top; ?>
	<div id="wrapper">
		<?php print $page; ?>
        <?php include './'. path_to_theme() .'/templates/page/footer.tpl.php'; ?>
    </div>
    <script type="text/javascript">
        var tsource = '<?php print t("Source"); ?>';
        var tcity = '<?php print t("City"); ?>';
        var tjob = '<?php print t("Job(s)"); ?>';
        var tsector = '<?php print t("Sector"); ?>';
        var tlanguage = '<?php print t("Language"); ?>';
        var tm1 = '<?php print t('January'); ?>';
        var tm2 = '<?php print t('February'); ?>';
        var tm3 = '<?php print t('March'); ?>';
        var tm4 = '<?php print t('April'); ?>';
        var tm5 = '<?php print t('May'); ?>';
        var tm6 = '<?php print t('June'); ?>';
        var tm7 = '<?php print t('July'); ?>';
        var tm8 = '<?php print t('August'); ?>';
        var tm9 = '<?php print t('September'); ?>';
        var tm10 = '<?php print t('October'); ?>';
        var tm11 = '<?php print t('November'); ?>';
        var tm12 = '<?php print t('December'); ?>';
        var tpassword = '<?php print t('Password <span class="txt-small">(8 characters minimum)</span> <span class="form-required" title="This field is required">*</span>'); ?>';
        var tcpassword = '<?php print t('Confirm your password <span title="This field is required." class="form-required">*</span>'); ?>';
        var tresume = '<?php print t("Your resume (<span>Accepted formats : Pdf, Doc, Docx</span>)"); ?>';
        var tpinformations = '<?php print t("Your personal information"); ?>';
        var tregister = '<?php print t("Register your new account"); ?>';
        var loginurl = '<?php print url("login"); ?>';
        var loadmore = '<?php print t('Load more'); ?>';
        var joindre = '<?php print t('Attach'); ?>';
        var phfn = '<?php print t('First name'); ?>';
        var phln = '<?php print t('Last name'); ?>';
    </script>
    <?php print $page_bottom; ?>
</body>

