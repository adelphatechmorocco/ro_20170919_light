
if( location.hash == "#_=_" ){
    location.href = location.href.split("#")[0];
    window.location.replace(location.protocol+"//"+location.host+"/cv-webhelp");
    //location.hash = "";
}
// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
if (!String.prototype.trim) {
    (function() {
        // Make sure we trim BOM and NBSP
        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
        String.prototype.trim = function() {
            return this.replace(rtrim, '');
        };
    })();
}

[].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
    // in case the input is already filled..
    if( inputEl.value.trim() !== '' ) {
        classie.add( inputEl.parentNode, 'input--filled' );
    }

    // events:
    inputEl.addEventListener( 'focus', onInputFocus );
    inputEl.addEventListener( 'blur', onInputBlur );
} );

function onInputFocus( ev ) {
    classie.add( ev.target.parentNode, 'input--filled' );
}

function onInputBlur( ev ) {
    if( ev.target.value.trim() === '' ) {
        classie.remove( ev.target.parentNode, 'input--filled' );
    }
}

/**
 * Author: Hafid Denguir // @hafid2com
 * jQuery Simple Parallax Plugin
 *
 */
(function($) {
 
        $.fn.parallax = function(options) {
     
            var windowHeight = $(window).height();
     
            // Establish default settings
            var settings = $.extend({
                speed        : 0.15,
                errorMargin  : 0
            }, options);
     
            // Iterate over each object in collection
            return this.each( function() {
     
                // Save a reference to the element
                var $this = $(this);
       
                // Set up Scroll Handler
                $(document).scroll(function(){
       
                var scrollTop = $(window).scrollTop();
                        var offset = $this.offset().top;
                        var height = $this.outerHeight();
             
                // Check if above or below viewport
                if (offset + height <= scrollTop || offset >= scrollTop + windowHeight) {
                    return;
                }
             
                var yBgPosition = Math.round((offset - scrollTop) * settings.speed);
             
                // Apply the Y Background Position to Set the Parallax Effect
                $this.css('background-position', 'center ' + (-yBgPosition-settings.errorMargin) + 'px'); 
                      
              });
            });
        }
}(jQuery));



(function($){
    var size_li = $(".wrapper-box-gray").size(),
    x=5;

    $('.wrapper-box-gray:lt('+x+')').show();
    $('.voir-plus a').click(function (event) {
        event.preventDefault();
        x= (x+5 <= size_li) ? x+5 : size_li;
        $('.wrapper-box-gray:lt('+x+')').slideDown( "slow" );
    });

    var news_size_li = $(".box-newsletter > ul > li").size(),
    news_x=9;

    $('.box-newsletter > ul > li:lt('+news_x+')').css("display","inline-block");
    $('.box-newsletter .buttons a').click(function (event) {
        event.preventDefault();
        news_x= (news_x+5 <= news_size_li) ? news_x+5 : news_size_li;
        $('.box-newsletter > ul > li:lt('+news_x+')').slideDown( "slow",function(){
            //if ($(this).is(':visible'))
                $(this).css('display','inline-block');
        } );
    });


    var hash = location.hash;

    if(hash == "#rabat"){
        setTimeout(function(){ $("a[data-ville="+hash.substring(1)+"]").click(); },1000);
    }

    if(hash == "#agadir"){
        setTimeout(function(){ $("a[data-ville="+hash.substring(1)+"]").click(); },1000);
    }

    if(hash == "#fes"){
        setTimeout(function(){ $("a[data-ville="+hash.substring(1)+"]").click(); },1000);
    }

    if( $(hash).length ){

        setTimeout(function(){
            $('html,body').animate({
                scrollTop: $(hash).offset().top
            }, 1000);
        },1000);

    }

    // detect navigators
    navigator.sayswho= (function(){
        var ua= navigator.userAgent, tem, 
        M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }
        if(M[1]=== 'Chrome'){
            tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
            if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }
        M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    })();
    var navigatorversion = parseInt( navigator.sayswho.split(' ')[1] ),
        navigatorName    = navigator.sayswho.split(' ')[0]; 

        if( (navigatorName == "Firefox" && navigatorversion<23) || (navigatorName == "Opera" && navigatorversion<15) || (navigatorName == "Safari" && navigatorversion<5)  || (navigatorName == "IE" && navigatorversion<11) ){

        $.fancybox({
            content      : $("#oldnavigator").unwrap(),
            minWidth     : ( $(window).width()>590 ) ? 590 : 320,
            minHeight    : ( $(window).width()>590 ) ? 345 : 420,
            width        : ( $(window).width()>590 ) ? 590 : 320,
            height       : ( $(window).width()>590 ) ? 345 : 420,
            padding      : 0,
            autoSize     : false,
            beforeShow: function(){
                $('body').append('<div class="fancybox-overlay fancybox-overlay-fixed fancybox-default-overlay"></div>');
                $('.fancybox-overlay').show();
            },
            afterShow: function(){
                // add text to close button
                $(".fancybox-close").html("×");
            },
            afterClose:function(){
                $('.fancybox-overlay').remove();
            }
        });
    }



    /*$('.espace-candidature').find('input[type="email"], input[type="text"]').keyup(function(){
        this.value = $(this).val().toLowerCase();
    });*/

    // GoTo
    $(".content-top-slide .main-menu a").click(function(){
        var id = $(this).attr("href").replace('/','');

        if( $(id).length ) {
            $("html,body").animate({ scrollTop: $(id).offset().top  }, 1000);
            return false;
        }

    });




    if($("#c-menu--slide-right").length){
        var slideRight = new Menu({
            wrapper: '#o-wrapper',
            type: 'slide-right',
            menuOpenerClass: '.c-button',
            maskId: '#c-mask'
        });

        var slideRightBtn = document.querySelector('#c-button--slide-right');
        slideRightBtn.addEventListener('click', function(e) {
            $('.slider-home .tp-bullets').css('z-index','-1');
            e.preventDefault;
            slideRight.open();
            
        });
   }



    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('actives');
        $(this).parents('.panel-heading').addClass('actives');

        $('.panel-title').removeClass('actives'); //just to make a visual sense
        $(this).parent().addClass('actives'); //just to make a visual sense
    });

    if($('.counter').length){
        /*$('.counter').each(function(){
            $(this).counterUp({
                delay: 10,
                time: 1000
            });
        });*/
    }

    if($('.wow').length){
        new WOW().init();
    }

    // slider page webhelp-maroc.html
    if($(".webhelp-maroc-carousel").length) {
            var slider, maxSlides,
                oBxSettings = {
                    slideMargin: 12,
                    minSlides: 3,
                    moveSlides: 1,
                    maxSlides: maxSlides,
                    slideWidth: 220,
                    auto: true,
                    pager:false
                };

            function init() {
                // Set maxSlides depending on window width
                oBxSettings.maxSlides = window.outerWidth < 800 ? 1 : 3;
            }

            $(document).ready(function () {
                init();
                // Initial bxSlider setup
                slider = $('.webhelp-maroc-carousel ul').bxSlider(oBxSettings);
            });

            $(window).resize(function () {
                // Update bxSlider when window crosses 430px breakpoint
                if ((window.outerWidth < 800 && window.prevWidth >= 800) || (window.outerWidth >= 800 && window.prevWidth < 800)) {
                    init();
                    slider.reloadSlider(oBxSettings);
                }
                window.prevWidth = window.outerWidth;
            });


        }
    if($(".fun-carousel").length) {
        var slider, maxSlides,
            oBxSettings = {
                slideMargin: 12,
                minSlides: 3,
                moveSlides: 1,
                maxSlides: maxSlides,
                slideWidth: 220,
                auto: true,
                pager:false
            };

        function init() {
            // Set maxSlides depending on window width
            oBxSettings.maxSlides = window.outerWidth < 800 ? 1 : 3;
        }

        $(document).ready(function () {
            init();
            // Initial bxSlider setup
            slider = $('.fun-carousel ul').bxSlider(oBxSettings);
        });

        $(window).resize(function () {
            // Update bxSlider when window crosses 430px breakpoint
            if ((window.outerWidth < 800 && window.prevWidth >= 800)
                || (window.outerWidth >= 800 && window.prevWidth < 800)) {
                init();
                slider.reloadSlider(oBxSettings);
            }
            window.prevWidth = window.outerWidth;
        });


    }

    $( ".close-ville" ).click(function() {
        $('.ville').fadeOut(1000);
    });

    /*Home page Slider*/
    if($("#owl-demo").length){
        /*$("#owl-demo").owlCarousel({
            autoPlay: 3000,
            singleItem:true
        });*/
        /*$("#owl-demo ul").bxSlider({
            controls: false,
            mode: "fade",
            speed: 1500,

        });*/

        jQuery('#owl-demo').revolution( {
            delay:9000,
            startwidth:1170,
            startheight:816,
            //navigationType:"none",
            navigationArrows:"none",
            fullScreen: "on",
        });
    }

    // slider page forum-metiers.html
    if( $(".forum-carousel").length ){
        var slider, maxSlides,
            oBxSettings = {
                pager: false,
                auto:true,
                slideMargin: 12,
                minSlides: 3,
                moveSlides: 1,
                maxSlides: maxSlides,
                slideWidth: 220,
            };

        function init() {
            // Set maxSlides depending on window width
            oBxSettings.maxSlides = window.outerWidth < 800 ? 1 : 3;
        }

        $(document).ready(function () {
            init();
            // Initial bxSlider setup
            slider = $('.forum-carousel ul').bxSlider(oBxSettings);
        });

        $(window).resize(function () {
            // Update bxSlider when window crosses 430px breakpoint
            if ((window.outerWidth < 800 && window.prevWidth >= 800)
                || (window.outerWidth >= 800 && window.prevWidth < 800)) {
                init();
                slider.reloadSlider(oBxSettings);
            }
            window.prevWidth = window.outerWidth;
        });


    }

    // slider page entraide.html
    if( $(".entraide-carousel").length ){
        var slider, maxSlides,
            oBxSettings = {
                pager: false,
                auto:true,
                slideMargin: 12,
                minSlides: 3,
                moveSlides: 1,
                maxSlides: maxSlides,
                slideWidth: 220
            };

        function init() {
            // Set maxSlides depending on window width
            oBxSettings.maxSlides = window.outerWidth < 800 ? 1 : 3;
        }

        $(document).ready(function () {
            init();
            // Initial bxSlider setup
            slider = $('.entraide-carousel ul').bxSlider(oBxSettings);
        });

        $(window).resize(function () {
            // Update bxSlider when window crosses 430px breakpoint
            if ((window.outerWidth < 800 && window.prevWidth >= 800)
                || (window.outerWidth >= 800 && window.prevWidth < 800)) {
                init();
                slider.reloadSlider(oBxSettings);
            }
            window.prevWidth = window.outerWidth;
        });


    }


    //delete placeholse on focus
    $('#srch-term').data('holder',$('#srch-term').attr('placeholder'));
    $('#srch-term').focusin(function(){
        $(this).attr('placeholder','');
    });
    $('#srch-term').focusout(function(){
        $(this).attr('placeholder',$(this).data('holder'));
    });


    $('#identifient').data('holder',$('#identifient').attr('placeholder'));
    $('#identifient').focusin(function(){
        $(this).attr('placeholder','');
    });
    $('#identifient').focusout(function(){
        $(this).attr('placeholder',$(this).data('holder'));
    });



    /*Home page Slider*/
    /*if($(".forum-carousel").length){
        $(".forum-carousel").owlCarousel({
            autoPlay:2000,
            dots: true,
            items: 1,
        });
    }*/

    if($("#quote-carousel2").length){
        $('#quote-carousel2').carousel({
            loop: true,
            pause: true,
            interval: false
        });
    }

    if($(".bxslider-stories").length) {
        $('.bxslider-stories').bxSlider({
            pagerCustom: '#bx-pager',
            mode: 'vertical',
            slideMargin: 5,
            controls: false,
            useCSS: false
        });
    }

    if($("#text-carousel").length) {
        $('#text-carousel ul').bxSlider({
            pager: false,
            auto: true,
            controls: false,
            mode: 'vertical',
            pause: 1000
        });
    }

    if($(".slider-videos").length) {
        $('.slider-videos ul').bxSlider({
            pager: false,
            useCSS: false, 
            auto:true
        });
    }


    /*jump to a specific section*/
/*    $('.main-menu a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
*/
    $('a[data-slide="prev"]').click(function(e) {
        $('#quote-carousel2').carousel('prev');
    });

    $('a[data-slide="next"]').click(function(e) {
        $('#quote-carousel2').carousel('next');
    });

    if($('#actujob').lengh){
        var topOfOthDiv = $('#actujob').offset().top;
        var $elNewsletter = $('#newsletter');
        var bottomOfDiv = $elNewsletter.offset().top; // + $elNewsletter.outerHeight(true);
        var shareBtnHeight = $('.share-btn').outerHeight(true);
        $(window).scroll(function() {
            if ((($(window).scrollTop() + (shareBtnHeight / 2)) > topOfOthDiv) && ($(window).scrollTop() < (bottomOfDiv + shareBtnHeight / 2))) {
                $("#dvid").show(200);
            } else {
                $("#dvid").hide(200);
            }
        });
    }

   
    //Display video youtube on click
    $(function() {
        if($(".video-link").length){
            $(".video-link").jqueryVideoLightning({
                autoplay: 1,
                backdrop_color: "#000",
                backdrop_opacity: 0.4,
                glow: 20,
                glow_color: "#000",
                autoclose: true,
                width: $(window).width(),
                height: $(window).height()
            });
        }
    });

    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        var villeID = getUrlParameter('field_ville_tid');
        var metierID = getUrlParameter('field_metier_tid');
        if( villeID ) {
            $('#edit-field-ville-tid option').each(function(){
              if ($(this).val() == villeID){
                $('#edit-field-ville-tid option:selected').removeAttr("selected");
                $(this).attr("selected","selected");
              }
            });
        }
        if( metierID ) {
            $('#edit-field-metier-tid option').each(function(){
              if ($(this).val() == metierID){
                $('#edit-field-metier-tid option:selected').removeAttr("selected");
                $(this).attr("selected","selected");
              }
            });
        }

        if( $('.msg-notfound').length ){
            $( '.title-not-found' ).removeClass("hidden");
            $( '.title-offre,.msg-notfound' ).hide();
        }
  
    // Stylize selectbox
    if($('.selectpicker,.form-select').length){

        $('.selectpicker').each(function(){
        
            var self = $(this),
                id = self.attr("id");

            if( id.indexOf("source")>-1 ) self.find("option[value='_none']").text("Source");
            if( id.indexOf("ville")>-1 ) self.find("option[value='All']").text("Ville");
            if( id.indexOf("region")>-1 ) self.find("option[value='_none']").text("Ville");
            if( id.indexOf("metier")>-1 )   self.find("option[value=All]").text("Métier(s)");

            $(this).select2({
                minimumResultsForSearch: -1
            });
        });

        $('.form-select').each(function(){

            var self = $(this),
                id = self.attr("id");

            if( id.indexOf("source")>-1 ) self.find("option[value='_none']").text("Source");
            if( id.indexOf("ville")>-1 ) self.find("option[value='All']").text("Ville");
            if( id.indexOf("region")>-1 ) self.find("option[value='_none']").text("Ville");
            if( id.indexOf("metier")>-1 )   self.find("option[value=All]").text("Métier(s)");

            $(this).select2({
                minimumResultsForSearch: -1
            });
        });

                

    }

    if($(".popin-selectpicker").length){
        function formatState (state) {
          if (!state.id) { return state.text; }
          var $state = $(
            '<span><img src="'+location.protocol+"//"+location.host+'/sites/all/themes/blankpage/images/flags/' + state.text.toLowerCase() + '.png" class="img-flag" /></span>'
          );
          return $state;
        };

        $(".popin-selectpicker").select2({
            minimumResultsForSearch: -1,
            templateResult: formatState,
            formatSelection: formatState
        });
    }


    // force slider to take height 100%
    /*var sliderHome = $("#owl-demo .item img");
    if(sliderHome.length){
        var h = $(window).height();
        sliderHome.height(h);

        $(window).resize(function(){
            var h = $(window).height();
            sliderHome.height(h);
        });        
    }*/

    // Add class fixed to fix menu on scroll
    if($(".menu-fixed").length){
        $(window).scroll(function(){ 
            if( $(window).scrollTop()>100 ){
                $(".top-slide").addClass("fixed");
            }else{
                $(".top-slide").removeClass("fixed");
            }
        });
    }

    var pages = ['.espace-candidature','.page-85','.nos-offres-actualites'];

    var noshare = 0;

    $.each(pages,function(indx,val){ 
        var _this = $(val).length;

        if( _this ) noshare++;
    });

    if( noshare>0 ) $( ".list-share" ).hide();


    if($("#dvid").length ){
        var scrollTop = ($(".slider-home").length>0)? 800 : 250;
        $(window).scroll(function(){ 
            if( $(window).scrollTop()>scrollTop ){
                $("#dvid").removeClass("hidden");
            }else{
                $("#dvid").addClass("hidden");
            }
        });
    }

    // Change date de naissance
    var dateNaiss = $("#edit-field-date-de-naissance-und-0-value-datepicker-popup-0,#edit-field-date-de-naissance-user-und-0-value-datepicker-popup-0"),
        dateNaissVal = dateNaiss.val();
        dND = (dateNaissVal) ? dateNaissVal.split('/')[0] : 0,
        dNM = (dateNaissVal) ? dateNaissVal.split('/')[1] : 0,
        dNY = (dateNaissVal) ? dateNaissVal.split('/')[2] : 0;

    var days = '<select name="day" id="dn-day" class="form-select">';
    var months = '<select name="month" id="dn-month" class="form-select">';
    var years = '<select name="years" id="dn-year" class="form-select">';

    for (var i = 1; i <= 31; i++) {
        var format = (i<9) ? '0'+i : i;
        var selected = (dND == format) ? "selected" : "";
        days+= '<option value="'+format+'" '+selected+'>'+format+'</option>';
    }

    var monthNames = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];

    for (var j = 0; j < monthNames.length; j++) {
        var format = (j<9) ? '0'+(j+1) : (j+1);
        var selected = (dNM == format) ? "selected" : "";
        months+= '<option value="'+format+'" '+selected+'>'+monthNames[j]+'</option>';
    }


    var ycurrent = new Date();
    for (var y = ycurrent.getFullYear(); y >= 1940 ; y--) {
        var format = (y<10) ? '0'+y : y;
        var selected = (dNY == format) ? "selected" : "";
        years+= '<option value="'+format+'" '+selected+'>'+format+'</option>';
    }

    days+='</select>';
    months+='</select>';
    years+='</select>';

    dateNaiss.after(days+months+years);

    if( $("#dn-day,#dn-month,#dn-year").length ){
        $("#dn-day,#dn-month,#dn-year").select2({
            minimumResultsForSearch: -1
        });
    }
    $("#dn-day,#dn-month,#dn-year").change(function(){
        var dateN = $("#dn-day").val()+"/"+$("#dn-month").val()+"/"+$("#dn-year").val();
        dateNaiss.val(dateN);
    });

    // Sync Email with username / Espace membre (register)
    jQuery("#edit-mail").on('propertychange change click keyup input paste', function(){
        var val = jQuery(this).val();
        jQuery(".username").val( val );
    });

    // set Interval to change text
    /*$(".form-type-managed-file .form-submit").on('click',function(){
        var label = $(this).parents('.form-type-managed-file').find("label:first");
        setInterval(function(){ 
            label.html("Ton CV (<span>á télécharger au format : Pdf, Doc, Docx</span>)"); 
        },100);
    });*/
    
    $("label[for='edit-pass-pass1']").html('Mot de passe <span class="txt-small">(8 caractères minimum)</span> <span class="form-required" title="Ce champ est obligatoire.">*</span>'); 


    $(".hybridauth-icon.facebook").addClass("fa fa-facebook");
    $(".hybridauth-icon.linkedin").addClass("fa fa-linkedin");
    $(".hybridauth-icon.google").addClass("fa fa-google-plus");

    $("#edit-pass-pass1").blur(function(){
      var val = $(this).val();
      if( val.length<8 ) $(this).css("background-color","#FFBABA");
    }).focus(function(){
      $(this).css("background-color","white");
    });
 
    $("label[for='edit-pass-pass2']").html('Confirmer ton mot de passe <span title="Ce champ est obligatoire." class="form-required">*</span>');

    $(".form-managed-file input[type='file']").on('change', function(){
        var _this = $(this).val();
        $(this).parent(".form-managed-file").attr('data-name',_this);
    });

    //
    $("#edit-field-cv-document,#edit-field-cv-document-und-0").find("label").html("Ton CV (<span>á télécharger au format : Pdf, Doc, Docx</span>)");
    $("#node_cv_webhelp_form_group_info_perso legend:first").find(".fieldset-legend").html("Tes informations personnelles")

    // hide/show button (postuler avec rappel) / page Espace condidature

    $("div[id*='field-type-de-poste'] input").each(function(){
        if($(this).is(":checked") && $(this).val()=="Conseiller client"){
            $(".buttons .postuler-rappel").addClass("zoomIn");
            return false;
        }
    });

    $("div[id*='field-type-de-poste'] input").click(function(){
        if($(this).is(":checked") && $(this).val()=="Conseiller client"){
            $(".buttons .postuler-rappel").addClass("zoomIn");
            $('.container-espace-candidature .info-poste label[for="edit-field-cv-document-und-0"] span').hide();
        }else{
            $(".buttons .postuler-rappel").removeClass("zoomIn");
            $('.container-espace-candidature .info-poste label[for="edit-field-cv-document-und-0"] span').show();
        }
    });

    $(".scroll-top").click(function(){
        $("html,body").animate({ scrollTop: 0  }, 1000);
        return false;
    });

    if($(".temoignage-monde").length){
        $(".temoignage-monde").parallax({
            speed: 0.5,
            errorMargin: 1000
        });        
    }

    if( $("#edit-newsletters-4").length ) $("#edit-newsletters-4").attr("checked","checked");
    if( $(".register #edit-submit").length ) $(".register #edit-submit").val("Crée ton nouveau compte");
    if( $("#field_cv_document_und_0_upload_button,#edit-field-cv-document-und-0-upload-button").length ) $("#field_cv_document_und_0_upload_button,#edit-field-cv-document-und-0-upload-button").val("Joindre");

    // on click sur le btn postuler submit le formulaire
    $(".espace-candidature .buttons .postuler").click(function(){
        var parent = $(this).parents(".espace-candidature");
        parent.find("#edit-submit").removeAttr("disabled").click();
    });
    // on click sur le btn postuler submit le formulaire
    $(".espace-candidature .buttons .postuler-rappel").click(function(){
        var parent = $(this).parents(".espace-candidature");
        parent.find("#edit-postulercond").removeAttr("disabled").click();
    });

    var registerItem = ",#edit-account"
    if( $(".container-espace-candidature.register").length ){
        $("#edit-account").wrapAll('<div class="info-poste"/>');

        $(".field-name-field-civilite,.field-name-field-nom,.field-name-field-prenom,.field-name-field-tel,.field-name-field-date-de-naissance-user,.field-name-field-conditions-utilisations, .group-info-perso").wrapAll('<div class="donnees-personnelles"/>');
        $(".container-espace-candidature.edition").find(".form-field-name-field-source,.field-name-field-source,.form-field-name-field-ville,.form-field-name-field-region,.field-name-field-region, .form-field-name-field-type-de-poste,#edit-account,.form-field-name-field-cv-document").wrapAll('<div class="info-poste"/>'); 

    }else{
        $(".field-name-field-civilite,.field-name-field-nom,.field-name-field-prenom,.field-name-field-tel,.field-name-field-date-de-naissance-user,#edit-account,.field-name-field-conditions-utilisations, .group-info-perso").wrapAll('<div class="donnees-personnelles"/>');

        $(".container-espace-candidature.edition").find(".form-field-name-field-source,.field-name-field-source,.form-field-name-field-ville,.form-field-name-field-region,.field-name-field-region, .form-field-name-field-type-de-poste,.form-field-name-field-cv-document").wrapAll('<div class="info-poste"/>'); 
    }
 

    $(".container-espace-candidature input[type='text']").each(function(){
        var regExp = /\edit-field-([^)]+)\-und/;
        var matches = regExp.exec($(this).attr("id"));
        var placeholder = (matches != null) ? matches[1].replace(/(cv-webhelp)|(-)|(user)/g,' ') : "";

        if( placeholder == "tel" ) placeholder = "+212";

        if (matches != null) $(this).attr('placeholder',placeholder);
    });


    if($(".slide-pages .top-slide, #owl-demo .tp-bgimg").length){
        $(".slide-pages .top-slide, #owl-demo .tp-bgimg").parallax({
            speed: 0.9,
            errorMargin: ( $(window).width() > 640 ) ? 0 : 26
        });        
    }

    // add opacity to slider caption
    $(window).scroll(function(){
        var top = $(window).scrollTop();
        if(top<600){
            var opacity = 1-(top/500)-0.1;
            $(".slide-pages .top-slide .titre,.slider-home .tp-caption .text-center,.slider-home .bottom-nav").css("opacity",(opacity>0) ? opacity : 0);            
        }

    });


    if(navigator.userAgent.indexOf('Mac') > 0)
        $('body').addClass('mac');

    // show hour bloc
    $(".link").click(function(){
        $(".call-phone").fadeOut("slow", function(){
            $(".call-hour").fadeIn("slow");
            return false;
        });
    });

    if ( $( ".page-52861" ).length ) $(".pages").find(".link-bien").addClass("active");
    if ( $( ".page-52860" ).length ) $(".pages").find(".link-avantage").addClass("active");
    if ( $( ".page-52866" ).length ) $(".pages").find(".link-respect").addClass("active");
    if ( $( ".entraide" ).length ) $(".pages").find(".link-entraide").addClass("active");
    if ( $( ".page-52876" ).length ) $(".pages").find(".link-fun").addClass("active");

    if ( $( ".page-52874" ).length ) $(".pages").find(".link-metiers").addClass("active");
    if ( $( ".page-52859" ).length ) $(".pages").find(".link-formation").addClass("active");
    if ( $( ".page-52864" ).length ) $(".pages").find(".link-orientation").addClass("active");
    if ( $( ".page-52867" ).length ) $(".pages").find(".link-evolution").addClass("active");

    // popin chat
    $(".zone button").click(function(){
        var txt = $(".zone textarea"),
        bx_msg = $(".box-chat .messages"),
            val = txt.val();

        if($.trim(val)){
            var html = '';
            html += '<div class="push left">';
            html += '    <span>'+val+'</span>';
            html += '</div>';

            bx_msg.append(html);
            txt.val("");
        }

        return false;
    });

    // show popin
    $(".share-btn a").click(function(){
        var box = $(this).attr("href").replace("#","");

        $(".popin .visible").removeClass("visible");
        if(box && $(".popin ."+box).length){
            $(".popin ."+box).addClass("visible");

            $(".popin").animate({ right:"0px" },1000);
            return false;
        }

    });

    // show popin
    $(".close-popin").click(function(){
        $(".popin").animate({ right:"-500px" },1000);

        return false;
    });

    // show popin map
    $(".fancybox").click(function(){
        var lng   = $(this).data("lng"),
            lat   = $(this).data("lat"),
            ville = $(this).data("ville"),
            pageMap = $(".villes-items").data("url-page") || '/node/52872',
            content = "<div id='fancymap'></div><div id='villes'></div>";

        $.fancybox.showLoading();

        $.fancybox({
            content      : content,
            minWidth     : 1300,
            minHeight    : 800,
            padding      : 0,

            fitToView    : false,
            width        : '95%',
            height       : '95%',
            autoSize     : false,
            afterShow: function(){
                setTimeout(function(){
                    // Call google Map
                    callMap(ville+","+lat+","+lng,"fancymap"); 

                    // Loading the bloc of villes in page map webhelp
                    $("#villes").load(pageMap+" #adresses-webhelp");

                    // show current ville info + hide fancybox loader 

                     setTimeout(function(){
                        $("#"+ville).show(); 
                        $.fancybox.hideLoading();
                    },1500); 

                },1500);

                // add text to close button
                $(".fancybox-close").html("×");
            }
        });
    });


    // Share buttons
    var title         = encodeURIComponent(document.title),
        url           = encodeURIComponent(document.location.href),
        image         = encodeURIComponent(jQuery('meta[property="og:image"]').attr("content")),
        description   = encodeURIComponent(jQuery('meta[name="description"]').attr("content")) || encodeURIComponent(jQuery('meta[property="og:description"]').attr("content")),
        descriptionTW = encodeURIComponent(jQuery('meta[name="description"]').attr("content")) || encodeURIComponent(jQuery('meta[name="twitter:description"]').attr("content")),
        siteTW        = encodeURIComponent(jQuery('meta[name="twitter:site"]').attr("content")),
        siteIMG        = encodeURIComponent(jQuery('meta[name="twitter:image"]').attr("content")),
        configWin     = 'toolbar=0,status=0,width=626,height=436';

        
        jQuery('meta[property="og:description"]').attr("content",jQuery('meta[name="description"]').attr("content"));
        
        //Share Facebook 
        jQuery(document).on('click', '.share-btn .facebook',function(e){
            e.preventDefault();

            var share_url = 'http://www.facebook.com/sharer.php';
            share_url+='?s=100&p[title]='+title+'&p[summary]='+description+'&p[url]='+url;
            share_url+='&p[images][0]='+image; 
            share_url+='&t='+title+'&e='+description;
            window.open(share_url + url, 'Share', configWin);
        });  


        jQuery(document).on('click', '.share-btn .twitter',function(e){
            e.preventDefault();
            //Share Twitter 
            if (descriptionTW.length > 90) {
                descriptionTW = descriptionTW.substr(0,90)+'…  ';
            };
 
            window.open('http://twitter.com/share?text='+descriptionTW + '&image-src='+siteIMG+'&url='+url + '&via=WebhelpMaroc'  , 'Share', configWin);
        });

        jQuery(document).on('click', '.share-btn .gplus',function(e){
            e.preventDefault();
 
            window.open('https://plus.google.com/share?url='+ url , 'Share', configWin);
        });

        jQuery(document).on('click', '.share-btn .linkedin',function(e){
            e.preventDefault();
 
            window.open('https://www.linkedin.com/shareArticle?mini=true&url='+ url +'&title='+ title +'&summary='+ description +'&source=Webhelp', 'Share', configWin); 
        });

        $(".espace-membre li > a").click(function(){
            if( $(this).hasClass("logged_in") ) return false;
            $.fancybox({
                minWidth     : ($('.logged_in').length) ? 380 : 750,
                minHeight    : ($('.logged_in').length) ? 450 : 630,
                padding      : 0,
                href: '/login',
                type: 'ajax', 
                afterShow: function(){
                    $(".fancybox-close").html("×");
                    $(".hybridauth-icon.facebook").addClass("fa fa-facebook");
                    $(".hybridauth-icon.linkedin").addClass("fa fa-linkedin");
                    $(".hybridauth-icon.google").addClass("fa fa-google-plus");
                }
            });
            return false;
        });


        $(".nano").each(function(){ 
            $(this).nanoScroller();
        });

        var vid = document.getElementById("videoIntro");
        if(vid != null) vid.volume = 0;

        if( $(".intitule-poste,.radio-civilite,.buttons").length )  $(".intitule-poste br,.radio-civilite br,.buttons br").remove()

    
    if( $(".actu-job .carousel-inner").length ){ 
        $(".actu-job .carousel-inner").owlCarousel({
            items:3,
            navigation:true,
            pagination:false,
            itemsDesktop : [1000,3],
            itemsDesktopSmall : [900,2],
            itemsTablet: [700,1],
            itemsMobile : [479,1]
        });
    }

    if( $('#slider-temoin').length ){
        $('#slider-temoin').bxSlider({
            mode: 'vertical',
            useCSS: false,
            auto: true,
            controls: false,
            pager: false
        });
    }

                    if($(".box-rse .chart").length){
                    $('.box-rse .chart').easyPieChart({
                        barColor:'#8bbe56',
                        size:100,
                        lineWidth : '9',
                        scaleColor: 'transparent'
        });

}





              $('.bien-etre-france .description').hover(
               function () {
                   $(this).parent().find('.description h4').css({"opacity":"0"});
                     $(this).parent().find('.description .description-item').css({"opacity":"1"});
               }, 
				 
               function () {
                        $(this).parent().find('.description h4').css({"opacity":"1"});
                     $(this).parent().find('.description .description-item').css({"opacity":"0"});
               }
            );

               $('.box-fun-france .box-fun-hover .fun-fr-box .description').hover(
               function () {
                   $(this).find('.first-text').css({"opacity":"0"});
                   $(this).find('.description-item').css({"opacity":"1"});
               }, 
				 
               function () {
                   $(this).parent().find(' .first-text').css({"opacity":"1"});
                   $(this).parent().find('.description-item').css({"opacity":"0"});
               }
            );


            $('.webhelp-france .our-values .items-values .item').hover(
               function () {
                   $(this).find('.img-value img').css({"opacity":"0"});
                   $(this).find('.text-hover p').css({"opacity":"1"});
               }, 
				 
               function () {
                   $(this).find('.img-value img').css({"opacity":"1"});
                   $(this).find('.text-hover p').css({"opacity":"0"});
               }
            );


             $('.bien-etre-portugal .container-bien-etre .img-bien-etre').hover(
               function () {
                   $(this).find('.text-hover').css({"opacity":"1"});
                   $(this).find('.outer-text-hover').css({"background":"#191919"});
                   $(this).find('.shadow').css({"opacity":"0"});
               }, 
				 
               function () {
                    $(this).find('.text-hover').css({"opacity":"0"});
                    $(this).find('.outer-text-hover').css({"background":"transparent"});
                    $(this).find('.shadow').css({"opacity":"1"});
               }
            );

            $('.box-fun-portugal .img-bien-etre').hover(
                function () {
                    $(this).find('.text-hover').css({"opacity":"1"});
                    $(this).find('.outer-text-hover').css({"background":"#191919"});
                    $(this).find('.shadow').css({"opacity":"0"});
                }, 
                    
                function () {
                        $(this).find('.text-hover').css({"opacity":"0"});
                        $(this).find('.outer-text-hover').css({"background":"transparent"});
                        $(this).find('.shadow').css({"opacity":"1"});
                }
            );

            
            $('.rse-fr-page .rse-fr-centre .box-rse .box-rse-left').hover(
               function () {
                   $(this).find('.box-rse-left-inner .text-hover').css({"opacity":"1"});
                   $(this).find('.box-rse-left-inner .text-premier').css({"opacity":"0"});
                   $(this).find('.box-rse-left-inner img').css({"opacity":"0"});
               }, 
				 
               function () {
                     $(this).find('.box-rse-left-inner .text-hover').css({"opacity":"0"});
                     $(this).find('.box-rse-left-inner .text-premier').css({"opacity":"1"});
                     $(this).find('.box-rse-left-inner img').css({"opacity":"1"});
               }
            );


            $('.rse-fr-page .rse-fr-centre .box-rse .box-rse-right').hover(
               function () {
                   $(this).find('.box-rse-right-inner').css({"opacity":"0"});
                   $(this).find('.text-hover').css({"opacity":"1"});
               }, 
				 
               function () {
                   $(this).find('.box-rse-right-inner').css({"opacity":"1"});
                   $(this).find('.text-hover').css({"opacity":"0"});
               }
            );

                        
            $('.box-fun-portugal .img-bien-etre').hover(
               function () {
                   $(this).find('h5').css({'top':'220px', 'transition':' top ease-in-out 0.5s'}); 
                   var posText = $(this).find('.text-hover').position();
                   var final = posText.top-20;
                   $(this).find('h5').css({'top':final});  
               }, 
				 
               function () {
                    $(this).find('h5').css({'top':'220px', 'transition':' top ease-in-out 0.5s'}); 
               }
            );


             $('.avantages-portugal-pages a').click(function(e){
                 e.preventDefault(); 
  })

    $(".flipper").flip({
  
    trigger: 'hover'
});



          $('.flip-container').hover(
               function () {
                   $(this).find('.back .inner-text').addClass('opacity-hover'); 
               }, 
				 
               function () {
                     $(this).find('.back .inner-text').removeClass('opacity-hover'); 
               }
            );


})(jQuery);
