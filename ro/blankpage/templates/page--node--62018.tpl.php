<!DOCTYPE html>
<!--[if IE 8]>   <html class="ie8"> <![endif]-->
<!--[if IE 9]>   <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Webhelp Côte d'ivoire</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/favicon.ico">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/style-menu-responsive.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/owl.carousel.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/owl.theme.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/home.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/main.css">
    <script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/modernizr.js"></script>
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/custom.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/mycustom.css">

</head>

<body class="aheight">
    <div id="wrapper">
        <div id="content">

            <div class="webhelp-france webhelp-maroc">

                 <div class="webhelp-maroc-content webhelp-france-content webhelp-portugal-content">
                <a href="<?php print url("<front>"); ?>" class="btn-back">Return to home page</a>
                <h1 class="text-center">Webhelp Madagascar</h1>
                <p class="leader-text text-center">En quelques chiffres:</p>
                <div class="temoignage-maroc temoignage-portugal">
                    <div class="temoignage-item">
                        <div class="img-temoignage">
                            <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/collaborateurs-maroc.png" class="center-block">
                        </div>
                        <div class="text-temoignage">
                            <span class="counter">300</span><span class="plus">+</span>
                            <span class="criteres-temoignage"> Employees</span>
                        </div>
                    </div>
                    <div class="temoignage-item">
                        <div class="img-temoignage">
                            <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-ville.png" class="center-block">
                        </div>
                        <div class="text-temoignage">
                            <span class="counter">1</span>
                            <span class="criteres-temoignage">Locations</span>
                        </div>
                    </div>
                    <div class="temoignage-item">
                        <div class="img-temoignage">
                            <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-client.png" class="center-block">
                        </div>
                        <div class="text-temoignage">
                            <span class="counter">10</span>
                            <span class="plus">+</span>
                            <span class="criteres-temoignage">Clients</span>
                        </div>
                    </div>
                    <div class="temoignage-item">
                        <div class="img-temoignage">
                            <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-lang.png" class="center-block">
                        </div>
                        <div class="text-temoignage">
                            <span class="counter">2</span>
                            <span class="criteres-temoignage">Languages</span>
                        </div>
                    </div>
                </div>
            </div>
                 <div class="our-values">
                        <h4 class="text-center">Nos valeurs</h4>
                        <p class="text-center">Nos 5 valeurs reflètent notre attitude et résument l’essence de nos actions :</p>
                        <div class="items-values">
                            <div class="item commitment">
                                <div class="img-value">
                                    <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-commitment.png" class="center-block" alt="">
                                </div>
                                <p>Engagement</p>
                                <div class="text-hover">

                                    <p> Pour nous, s’engager signifie tenir sans faille nos promesses, envers les collaborateurs du groupe mais aussi envers nos clients</p>
                                </div>
                            </div>
                            <div class="item recognition">
                                <div class="img-value">
                                    <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-recognition.png" class="center-block"  alt="">
                                </div>
                                <p>Reconnaissance</p>
                                 <div class="text-hover">
                                    <p>Chez Webhelp, nous reconnaissons le travail accompli, respectons la contribution et valorisons la réussite de chacun autour de nous. </p>
                                </div>
                            </div>
                            <div class="item unity">
                                <div class="img-value">
                                    <img class="center-block" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-unity.png"  alt="">
                                </div>
                                <p>Unité</p>
                                <div class="text-hover">
                                    <p>Les équipes n’ont d’autres objectifs que la satisfaction du travail accompli : chaque jour est une petite bataille mais au final, unis nous sommes fiers de nos victoires sur le long terme.</p>
                                </div>
                            </div>
                            <div class="item integrity">
                                <div class="img-value">
                                    <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-integrity.png" class="center-block"  alt="">
                                </div>
                                <p>Exemplarité</p>
                                 <div class="text-hover">
                                    <p>Nous mettons tout en œuvre pour créer un environnement de travail le plus agréable possible. Par notre attitude et notre intégrité, nous montrons l’exemple en toute circonstance ! </p>
                                </div>
                            </div>
                            <div class="item woww">
                                <div class="img-value">
                                    <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-wow.png" class="center-block" alt="">
                                </div>
                                <p>Effet ‘Wahou’</p>
                                 <div class="text-hover">
                                    <p>Nous cherchons sans cesse à créer l’heureuse surprise et à nous dépasser pour les personnes avec lesquelles nous travaillons.
</p>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="webhelp-ville">
                    <p class="text-center">Nos implantations :</p>
                    <div class="villes-items villes-items-ci " data-url-page="/mada/node/62025?v=2">,
                        <div class="ville-nom">
                            <a href="#" class="fancybox" data-ville="Antananarivo" data-lat="4" data-lng="10" ><p>Antananarivo</p></a>
                        </div>
                    </div>
                        <div class="slider-locaux">
                            <p class="text-center">Nos locaux modernes et de haut standing :</p>
                            <div class="webhelp-maroc-carousel">
                                <ul>
                                    <li> <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mediatheque/m1.jpg" class="center-block"> </li>
                                    <li> <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mediatheque/m2.jpg" class="center-block"> </li>
                                    <li> <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mediatheque/m3.jpg" class="center-block"> </li>
                                    <li> <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mediatheque/m4.jpg" class="center-block"> </li>
                                    <li> <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mediatheque/m5.jpg" class="center-block"> </li>
                                    <li> <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mediatheque/m6.jpg" class="center-block"> </li>
                                    <li> <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mediatheque/m7.jpg" class="center-block"> </li>
                                    <li> <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mediatheque/m8.jpg" class="center-block"> </li>
                                    <li> <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mediatheque/m9.jpg" class="center-block"> </li>


                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/menu.js"></script>
<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/owl.carousel.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.counterup.js"></script>
<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBbnehLajrgSd0gL4THGOD0O7DXmBa2Ci4&v=1"></script>
<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/script.js?v=2"></script>
<script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.easypiechart.min.js"></script>
<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/main.js?v=4"></script>
</body>

</html>
