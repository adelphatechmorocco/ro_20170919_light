<!DOCTYPE html>
<!--[if IE 8]>   <html lang="fr" class="ie8"> <![endif]-->
<!--[if IE 9]>   <html lang="fr" class="ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="fr">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Espace de candidature</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/style-menu-responsive.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/select2.min.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/home.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/main.css">
    <script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/modernizr.js"></script>
</head>

<body>
<div class="content-404">
    <div class="text-404">
        <h1>404</h1>
        <h5>Oupsss.... page introuvable :/</h5>
        <p>La page que vous avez demandée n'a pas été trouvée. Il se peut que le lien que vous avez utilisé sont rompu ou que ayez tapé l'adresse<br/>(URL) incorrectement. Vous avez maintenant la possibilité suivante : <br/>Revenir à <a href="/">la page d'accueil</a>.</p>

    </div>
</div>
    <script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/wow.min.js"></script>
    <script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/select2.full.min.js"></script>
    <script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/menu.js"></script>
<script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.easypiechart.min.js"></script>
    <script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/main.js"></script>
</body>

</html>
