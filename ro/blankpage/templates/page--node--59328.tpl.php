<?php /*************** NOS METIERS EN ***************/ ?>
<!DOCTYPE html>
<!--[if IE 8]>   <html class="ie8"> <![endif]-->
<!--[if IE 9]>   <html class="ie9"> <![endif]-->
<!--[if gt IE 9]>
<html>
<!<![endif]-->

<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Profesii</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/favicon.ico">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/style-menu-responsive.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/home.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/select2.min.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/main.css">
    <script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/modernizr.js"></script>
</head>

<body class="login">
<div id="wrapper">
    <div id="content">
        <div class="box-metier">
            <div class="header">
                <a href="<?php print url("<front>")?>#avenir" class="btn-back">Retour a l'accueil</a>
                <h2>Activităţile noastre</h2>
                <p>
                   Webhelp înseamnă peste 120 de posibilităţi de carieră
                </p>
            </div>
            <div class="slider-videos">
                <ul class="list-videos">
                        <li>
						<div class="pushs">

                    <div class="push">
                        <img src="http://img.youtube.com/vi/OpvljXY_Qeo/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="OpvljXY_Qeo" data-video-width="1100" data-video-height="650"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Ancuta</h2>
                            <p>Manager de Operaţiuni</p>
                        </div>
                    </div> <!-- push -->
                    <div class="push">
                        <img src="http://img.youtube.com/vi/35eb140R1ZM/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="35eb140R1ZM" data-video-width="1100" data-video-height="650"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Andrei</h2>
                            <p>Head of Planning and Reporting</p>
                        </div>
                    </div> <!-- push -->
                    <div class="push">
                        <img src="http://img.youtube.com/vi/YH5OprVCEys/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="YH5OprVCEys" data-video-width="1100" data-video-height="650"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Diana</h2>
                            <p>Head of Recruitment and Mobility</p>
                        </div>
                    </div> <!-- push -->
                    <div class="push">
                        <img src="http://img.youtube.com/vi/8aQqtKpNLzc/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="8aQqtKpNLzc" data-video-width="1100" data-video-height="650"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Georgiana</h2>
                            <p>Şef de proiect</p>
                        </div>
                    </div> <!-- push -->
                    <div class="push">
                        <img src="http://img.youtube.com/vi/1JMH6mkvb5E/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="1JMH6mkvb5E" data-video-width="1100" data-video-height="650"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Madalina</h2>
                            <p>Şef de proiect Ameliorare Continuă</p>
                        </div>
                    </div> <!-- push -->
                    <div class="push">
                        <img src="http://img.youtube.com/vi/Za6fq1_dKzY/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="Za6fq1_dKzY" data-video-width="1100" data-video-height="650"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Vitty</h2>
                            <p>Chef de Projets</p>
                        </div>
                    </div> <!-- push -->
                  
                    <div class="push">
                        <img src="http://img.youtube.com/vi/L7sPfAcrEWI/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="L7sPfAcrEWI" data-video-width="1100" data-video-height="650"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Bartosz</h2>
                            <p>Expert Métier</p>
                        </div>
                    </div>
					
					<div class="push">
                        <img src="http://img.youtube.com/vi/iQRXURV8vXQ/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="iQRXURV8vXQ" data-video-width="1100" data-video-height="650"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Raluca</h2>
                            <p>Manager de Operaţiuni</p>
                        </div>
                    </div> 
					
					
					 <div class="push">
                        <img src="http://img.youtube.com/vi/uyj53dEj6YI/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="uyj53dEj6YI" data-video-width="1100" data-video-height="650"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Alexandre</h2>
                            <p>Consilier Client</p>
                        </div>
                    </div>
					
					<!-- push -->
                </div> <!-- pushs -->
                
                </li>
                </ul>
            </div><!-- slider-videos -->
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/wow.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.bxslider.js"></script>
<script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/videoLightning.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/menu.js"></script>
<script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.easypiechart.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/main.js"></script>
</body>

</html>
