<!DOCTYPE html>
<!--[if IE 8]>   <html class="ie8"> <![endif]-->
<!--[if IE 9]>   <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Webhelp Romania</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/favicon.ico">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/style-menu-responsive.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/home.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/main.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/custom.css">

<body class="map">
<div id="wrapper" class="wrapper">
    <div id="content">
        <div id="map"></div>
        <div class="info-window">test info window</div>
        <div class="logo-bottom">
            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/logo.png">
        </div>
        <div class="adresses-webhelp" id="adresses-webhelp">
           
              <div class="ville" id="bucarest1">
                    <i class="fa fa-times close-ville" aria-hidden="true"></i>
                        <div class="content-ville">
                        <a href="<?php print url("offre-emploi", array('query' => array('field_ville_tid' => '69' ))); ?>" class="btn-new">Opportunities</a>
                        <h3 class="text-center" style="padding-bottom: 61px !important;">Bucharest I</h3>
                        <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/villes/bucarest_1.jpg" alt="" />
                        <p>Jupiter House, Sfintii Voievozi Street, 65<br />
	                        010965 Bucharest<br>
                            Romania
                        </p>
                     </div>
                </div>
                <div class="ville" id="bucarest2">
                    <i class="fa fa-times close-ville" aria-hidden="true"></i>
                    <div class="content-ville">
                        <a href="<?php print url("offre-emploi", array('query' => array('field_ville_tid' => '69' ))); ?>" class="btn-new">Opportunities</a>
                         <h3 class="text-center" style="padding-bottom: 61px !important;">Bucharest II</h3>
                        <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/villes/bucarest_2_2.jpg" alt="" />
                        <p>Premium Plaza, Dr. Iacob Felix Street, 63-69<br>
                            011033 Bucharest<br>
							Romania</p>
                     </div>
                </div>
				
				<div class="ville" id="iasi">
                    <i class="fa fa-times close-ville" aria-hidden="true"></i>
                    <div class="content-ville">
                        <a href="<?php print url("offre-emploi", array('query' => array('field_ville_tid' => '70' ))); ?>" class="btn-new">Opportunities</a>
                        <h3 class="text-center" style="padding-bottom: 61px !important;">Iasi</h3>
                        <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/villes/iasi_ext2.jpg" alt="" />
                        <p>Moldova Center building, 5th Floor, Palat Street, 1<br>
                            Iasi<br>
							Romania</p>
                     </div>
                </div>
	
				<div class="ville" id="ploiesti">
                    <i class="fa fa-times close-ville" aria-hidden="true"></i>
                    <div class="content-ville">
                        <a href="<?php print url("offre-emploi", array('query' => array('field_ville_tid' => '71' ))); ?>" class="btn-new">Opportunities</a>
                         <h3 class="text-center" style="padding-bottom: 61px !important;">Ploiesti</h3>
                        <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/villes/ploiesti_ext.jpg" alt="" />
                        <p>Hipodrom Office Center, 4th floor, Blvd Bucharest, 39 A<br>
                            Ploiesti<br>
							Roumanie</p>
                     </div>
                </div>
	
				<div class="ville" id="galati">
                    <i class="fa fa-times close-ville" aria-hidden="true"></i>
                    <div class="content-ville">
                        <a href="<?php print url("offre-emploi", array('query' => array('field_ville_tid' => '72' ))); ?>" class="btn-new">Opportunities</a>
                         <h3 class="text-center" style="padding-bottom: 61px !important;">Galati</h3>
                        <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/villes/galati_ext.jpg" alt="" />
                        <p>George Cosbuc Street, 116-118<br>
							800385 Galati<br>
							Romania</p>
                     </div>
                </div>

        </div>
    </div>
</div>

<div id="wrapper" class="wrapper">
    <div class="map-pays-page">
        <div class="google-map" id="map">
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap.min.js"></script>
<script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.easypiechart.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/main.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/script.js"></script>
<script>
</script>
</body>

</html>
