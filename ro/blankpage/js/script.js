var styles =  [{"featureType":"road","elementType":"all","stylers":[{"visibility":"on"},{"color":"#000000"},{"hue":"#000000"},{"saturation":100},{"lightness":100},{"gamma":0.01}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"simplified"},{"color":"#000000"},{"hue":"#752b75"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#000000"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#0092e0"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"},{"invert_lightness":false},{"color":"#c6e376"},{"weight":0.1},{"gamma":1.03}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"color":"#4a4a4a"}]},{"featureType":"road","elementType":"all","stylers":[{"color":"#dae5eb"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f5fcff"}]},{"featureType":"transit.line","elementType":"all","stylers":[{"visibility":"off"},{"invert_lightness":false}]}];

var locations = [
    ['bucarest1', 44.4465999, 26.0871563],
    ['bucarest2', 44.4507421, 26.0792585],
    ['iasi', 47.160029, 27.5853147],
    ['ploiesti', 44.9122412,26.0369732],
    ['galati', 45.4362136, 28.034512],
  
 
];

var map,
    markers = [];

function callMap(markers, id) {
    var mapID = (id) ? id : 'map',
        num_markers = locations,
        lat = 47.902964,
        lng = 1.909251,
        r = [];

    if(markers){
        r = markers.split(",");
        //num_markers = new Array(r);

        lat = num_markers[0][1];       
        lng = num_markers[0][2];       
    }

    map = new google.maps.Map(document.getElementById(mapID), {
        zoom: ($(window).width()<1440) ? 5 : 6,
        center: new google.maps.LatLng(lat,lng), //31.79170, -7.09262
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: styles,
        disableDefaultUI: true,
        zoomControl: true
    });
    bounds  = new google.maps.LatLngBounds();
     
    if(( typeof markers) !=  undefined)
        addAllMarkers(num_markers,r);
    else
        addAllMarkers();

}

function addAllMarkers(num_markers,r){  
	 
    for (var i = 0; i < num_markers.length; i++) {

        markers[i] = new google.maps.Marker({
            position: { lat: parseFloat(num_markers[i][1]), lng: parseFloat(num_markers[i][2]) },
            map: map,
            html: num_markers[i][0],
            id: i,
            icon: {
                url: (r[0] == num_markers[i][0]) ? "http://"+location.host+"/sites/all/themes/blankpage/images/orange-marker.png" : "http://"+location.host+"/sites/all/themes/blankpage/images/blue-marker.png",
            },

        });

        /*loc = new google.maps.LatLng(parseFloat(num_markers[i][1]), parseFloat(num_markers[i][2]));
        bounds.extend(loc);*/

        var infowindow = new google.maps.InfoWindow({
            content: num_markers[i][0]
        });


        google.maps.event.addListener(markers[i], 'click', function() {  
            for (var i = 0; i < num_markers.length; i++) {
                markers[i].setIcon("http://"+location.host+"/sites/all/themes/blankpage/images/blue-marker.png");
            }

            var _this = this;
                _this.setIcon("http://"+location.host+"/sites/all/themes/blankpage/images/orange-marker.png");
            
            var ville = this.html;

            $(".ville").fadeOut("slow", function(){
               setTimeout(function(){
                $("#"+ville).fadeIn();
               },500); 
            });

            map.setCenter(this.getPosition());
            map.setZoom(17);

            google.maps.event.addListenerOnce(infowindow, 'closeclick', function() {
                markers[this.id].setVisible(true);
            });
            //console.log(markers[this.id]);

            //map.setCenter(latlngbounds.getCenter());
        });
    }
}

callMap();

$(window).resize(function(){
    callMap();
});
